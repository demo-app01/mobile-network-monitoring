import moment from 'moment';
import 'moment-timezone';
// import alertTypes from './pages/alerts/alertTypes-config'
class Utils {

    static instance = null;
    static createInstance() {
        var object = new Utils();
        return object;
    }

    static init(){
        if (!Utils.instance) {
            Utils.instance = Utils.createInstance();
        }
    }

    static round(data, decimals)
    {
        return Number(Math.round(data*Math.pow(10,decimals || 2))/Math.pow(10,decimals || 2))
    }
        
}

export default Utils;