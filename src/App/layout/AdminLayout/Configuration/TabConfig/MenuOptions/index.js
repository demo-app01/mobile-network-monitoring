import React, {Component} from 'react';
import {connect} from 'react-redux';
import PerfectScrollbar from 'react-perfect-scrollbar';

import Aux from "../../../../../../hoc/_Aux";
import * as actionTypes from "../../../../../../store/actions";
import DEMO from "../../../../../../store/constant";

class MenuOptions extends Component {
    render() {
        let menuOptions = '';
        if (this.props.layout !== 'horizontal') {
            menuOptions = (
                <div>
                    <h6>Menu Title Color</h6>
                    <div className="theme-color title-color small">
                        <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavListTitleColor('title-default')} className={this.props.navListTitleColor === 'title-default' ? 'active' : ''} data-value="title-default"><span/><span/></a>
                        <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavListTitleColor('title-blue')} className={this.props.navListTitleColor === 'title-blue' ? 'active' : ''} data-value="title-blue"><span/><span/></a>
                        <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavListTitleColor('title-red')} className={this.props.navListTitleColor === 'title-red' ? 'active' : ''} data-value="title-red"><span/><span/></a>
                        <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavListTitleColor('title-purple')} className={this.props.navListTitleColor === 'title-purple' ? 'active' : ''} data-value="title-purple"><span/><span/></a>
                        <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavListTitleColor('title-lightblue')} className={this.props.navListTitleColor === 'title-lightblue' ? 'active' : ''} data-value="title-lightblue"><span/><span/></a>
                        <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavListTitleColor('title-dark')} className={this.props.navListTitleColor === 'title-dark' ? 'active' : ''} data-value="title-dark"><span/><span/></a>
                    </div>
                    <div className="form-group mb-0">
                        <div className="switch switch-primary d-inline m-r-10">
                            <input type="checkbox" id="caption-hide" checked={this.props.navListTitleHide} onChange={this.props.onChangeNavListTitleHide} />
                            <label htmlFor="caption-hide" className="cr" />
                        </div>
                        <label>Menu Title Hide</label>
                    </div>
                </div>
            );
        }
        return (
            <Aux>
                <div className="config-scroll">
                    <PerfectScrollbar>
                        <h6>Menu Dropdown Icon</h6>
                        <div className="theme-color">
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in-1" id="drpicon-1" checked={this.props.navDropdownIcon === 'style1'} onChange={() => this.props.onChangeNavDropDownIcon('style1')} />
                                    <label htmlFor="drpicon-1" className="cr"><i className="feather icon-chevron-right"/></label>
                                </div>
                            </div>
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in-1" id="drpicon-2" checked={this.props.navDropdownIcon === 'style2'} onChange={() => this.props.onChangeNavDropDownIcon('style2')} />
                                    <label htmlFor="drpicon-2" className="cr"><i className="feather icon-chevrons-right"/></label>
                                </div>
                            </div>
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in-1" id="drpicon-3" checked={this.props.navDropdownIcon === 'style3'} onChange={() => this.props.onChangeNavDropDownIcon('style3')} />
                                    <label htmlFor="drpicon-3" className="cr"><i className="feather icon-plus"/></label>
                                </div>
                            </div>
                        </div>
                        <h6>Menu List Icon</h6>
                        <div className="theme-color">
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in" id="subitem-1" checked={this.props.navListIcon === 'style1'} onChange={() => this.props.onChangeNavListIcon('style1')}/>
                                    <label htmlFor="subitem-1" className="cr"><i className=" "/></label>
                                </div>
                            </div>
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in" id="subitem-2" checked={this.props.navListIcon === 'style2'} onChange={() => this.props.onChangeNavListIcon('style2')}/>
                                    <label htmlFor="subitem-2" className="cr"><i className="feather icon-minus"/></label>
                                </div>
                            </div>
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in" id="subitem-3" checked={this.props.navListIcon === 'style3'} onChange={() => this.props.onChangeNavListIcon('style3')}/>
                                    <label htmlFor="subitem-3" className="cr"><i className="feather icon-check"/></label>
                                </div>
                            </div>
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in" id="subitem-4" checked={this.props.navListIcon === 'style4'} onChange={() => this.props.onChangeNavListIcon('style4')}/>
                                    <label htmlFor="subitem-4" className="cr"><i className="icon feather icon-corner-down-right"/></label>
                                </div>
                            </div>
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in" id="subitem-5" checked={this.props.navListIcon === 'style5'} onChange={() => this.props.onChangeNavListIcon('style5')}/>
                                    <label htmlFor="subitem-5" className="cr"><i className="icon feather icon-chevrons-right"/></label>
                                </div>
                            </div>
                            <div className="form-group d-inline">
                                <div className="radio radio-primary d-inline">
                                    <input type="radio" name="radio-in" id="subitem-6" checked={this.props.navListIcon === 'style6'} onChange={() => this.props.onChangeNavListIcon('style6')}/>
                                    <label htmlFor="subitem-6" className="cr"><i className="icon feather icon-chevron-right"/></label>
                                </div>
                            </div>
                        </div>
                        <h6>Active Color</h6>
                        <div className="theme-color active-color small">
                            <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavActiveListColor('active-default')} className={this.props.navActiveListColor === 'active-default' ? 'active' : ''} data-value="active-default"><span/><span/></a>
                            <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavActiveListColor('active-blue')} className={this.props.navActiveListColor === 'active-blue' ? 'active' : ''} data-value="active-blue"><span/><span/></a>
                            <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavActiveListColor('active-red')} className={this.props.navActiveListColor === 'active-red' ? 'active' : ''} data-value="active-red"><span/><span/></a>
                            <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavActiveListColor('active-purple')} className={this.props.navActiveListColor === 'active-purple' ? 'active' : ''} data-value="active-purple"><span/><span/></a>
                            <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavActiveListColor('active-lightblue')} className={this.props.navActiveListColor === 'active-lightblue' ? 'active' : ''} data-value="active-lightblue"><span/><span/></a>
                            <a href={DEMO.BLANK_LINK} onClick={() => this.props.onChangeNavActiveListColor('active-dark')} className={this.props.navActiveListColor === 'active-dark' ? 'active' : ''} data-value="active-dark"><span/><span/></a>
                        </div>
                        {menuOptions}
                    </PerfectScrollbar>
                </div>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        layout: state.layout,
        navDropdownIcon: state.navDropdownIcon,
        navListIcon: state.navListIcon,
        navActiveListColor: state.navActiveListColor,
        navListTitleColor: state.navListTitleColor,
        navListTitleHide: state.navListTitleHide
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onChangeNavDropDownIcon: (navDropdownIcon) => dispatch({type: actionTypes.NAV_DROPDOWN_ICON, navDropdownIcon: navDropdownIcon}),
        onChangeNavListIcon: (navListIcon) => dispatch({type: actionTypes.NAV_LIST_ICON, navListIcon: navListIcon}),
        onChangeNavActiveListColor: (navActiveListColor) => dispatch({type: actionTypes.NAV_ACTIVE_LIST_COLOR, navActiveListColor: navActiveListColor}),
        onChangeNavListTitleColor: (navListTitleColor) => dispatch({type: actionTypes.NAV_LIST_TITLE_COLOR, navListTitleColor: navListTitleColor}),
        onChangeNavListTitleHide: () => dispatch({type: actionTypes.NAV_LIST_TITLE_HIDE})
    }
};

export default connect(mapStateToProps, mapDispatchToProps) (MenuOptions);