import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Tabs, Tab} from 'react-bootstrap';

import ColorOptions from './ColorOptions';
import LayoutOptions from './LayoutOptions';
import MenuOptions from './MenuOptions';
import Aux from "../../../../../hoc/_Aux";

class TabConfig extends Component {
    render() {
        let layout, color;
        let defaultActive;
        if (this.props.layout !== 'horizontal') {
            layout = (
                <Tab eventKey="layout" title="LAYOUT">
                    <LayoutOptions/>
                </Tab>
            );
        }

        if (this.props.preLayout !== 'layout-8') {
            defaultActive = 'color';
            color = (
                <Tab eventKey="color" title="COLOR">
                    <ColorOptions />
                </Tab>
            );
        } else {
            defaultActive = 'extra';
        }

        return (
            <Aux>
                <Tabs variant="pills" defaultActiveKey={defaultActive}  id="pills-custom-tab" className="mb-2">
                    {color}
                    {layout}
                    <Tab eventKey="extra" title="EXTRA">
                        <MenuOptions />
                    </Tab>
                </Tabs>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        layout: state.layout,
        preLayout: state.preLayout
    }
};

export default connect(mapStateToProps) (TabConfig);