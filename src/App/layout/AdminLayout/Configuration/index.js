import React, {Component}  from 'react';
import {connect} from 'react-redux';

import Layout from './Layout';
import PreBuiltLayout from './PreBuiltLayout';
import TabConfig from './TabConfig';
import Aux from "../../../../hoc/_Aux";
import DEMO from "../../../../store/constant";
import * as actionTypes from "../../../../store/actions";

class Configuration extends Component {
    state = {
        configOpen: false
    };

    render() {
        let configClass = ['menu-styler'];
        if (this.state.configOpen) {
            configClass = [...configClass, 'open'];
        }

        return (
            <Aux>
                <div id="styleSelector" className={configClass.join(' ')}>
                    <div className="style-toggler">
                        <a href={DEMO.BLANK_LINK} onClick={() => this.setState(prevState => {return {configOpen: !prevState.configOpen}})}>*</a>
                    </div>
                    <div className="style-block">
                        {/* <h6 className="mb-2">Datta Able Live Menu Customizer</h6> */}
                        <h6 className="mb-2">Live Menu Customizer</h6>
                        <hr className="my-0"/>
                        <Layout />
                        <PreBuiltLayout />

                        {/* icon colored */}
                        <div className="form-group mb-3">
                            <div className="switch switch-primary d-inline m-r-10">
                                <input type="checkbox" id="icon-colored" checked={this.props.navIconColor} onChange={this.props.onIconColored} />
                                <label htmlFor="icon-colored" className="cr" />
                            </div>
                            <label>Icon Color</label>
                        </div>

                        <TabConfig />
                    </div>
                </div>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        navIconColor: state.navIconColor,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onIconColored: () => dispatch({type: actionTypes.NAV_ICON_COLOR}),
    }
};

export default connect(mapStateToProps, mapDispatchToProps) (Configuration);