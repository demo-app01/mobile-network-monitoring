import React from "react";

export default ({size}) => (
	size ?
	<div className="auth-wrapper">
		<div className="auth-content" style={{ textAlign: 'center' }}>
			<div className="spinner-border mr-0" role="status" style={{ width: size + "rem", height: size + "rem" }}><span className="sr-only">Loading...</span></div>
		</div>
	</div> : <div className="spinner-border mr-0" role="status"><span className="sr-only">Loading...</span></div>
);
