import React, { Component } from 'react';
import SinglePlant from './SinglePlant'

class PlantGridList extends Component {
    render() {
        const { Plants } = this.props;
        return (
            <div>
                { Object.keys(this.props.Plants).map((plant, index) => (
                    <SinglePlant key={index} elementAction={this.props.elementAction} plant={Plants[plant]} />
                )) }
            </div>
        );
    }
}

export default PlantGridList;