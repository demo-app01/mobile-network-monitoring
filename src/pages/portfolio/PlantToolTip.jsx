import React, { Component } from 'react';
import Utils from '../../Utils';
import { Row, Col, Card, Badge } from 'react-bootstrap';
// import plantImage from '../../assets/images/solar/plant.png'
class PlantToolTip extends Component {
    render() {
        const { plant, type } = this.props;
        return (
            <a href={`/${plant.plantName}/dashboard`}>
                <Card className="m-0 shadow-md">
                    <Card.Header className={`avatar ${plant.statusColor ? plant.statusColor : 'default'}`} style={{ padding: '15px' }}>
                        <div className='d-flex justify-content-between align-items-center'>
                            <span className={`${plant.statusColor === 'blackOutline' || plant.statusColor === 'stage_4'  ? 'text-dark': 'text-light'}`}>{ plant.plantDisplayName }</span>
                            <div className="d-flex align-items-center">
                                {/* <Badge pill variant="light" style={{ padding: '6px 10px', lineHeight: '0.9' }} className={ plant && (plant.PR > 0 && plant.PR < 100 || !plant.PR) ? 'mr-1' : 'text-danger mr-1' }>
                                    PR : { plant && (plant.PR || plant.PR === 0) ? Utils.round(plant.PR, 2) : 'NA' } %
                                </Badge> */}
                            </div>
                        </div>
                    </Card.Header>
                    <Card.Body style={{ padding: '20px 15px'}}>
                        <Row className="align-items-center justify-content-center">
                            <Col sm={6} className="text-center border-right my-2" style={{ padding: '0 10px'}}>
                                <h6 className="f-w-300 m-0 mb-1" style={{ letterSpacing: '1px'}}>
                                    { plant && (plant.insolationPOA || plant.insolationPOA === 0) ? Utils.round(plant.insolationPOA, 2) : 'NA' }
                                </h6>
                                <span className="d-block f-10">5G</span>
                            </Col>
                            <Col sm={6} className="text-center my-2" style={{ padding: '0px 10px'}}>
                                <h6 className="f-w-300 m-0 mb-1" style={{ letterSpacing: '1px'}}>
                                    { plant && (plant.exportToday || plant.exportToday === 0) ? Utils.round(plant.exportToday/1000, 2) : 'NA' }
                                </h6>
                                <span className="d-block f-10">3G</span>
                            </Col>
                            <Col sm={6} className="text-center border-right my-2" style={{ padding: '0 10px'}}>
                                <h6 className="f-w-300 m-0 mb-1" style={{ letterSpacing: '1px'}}>
                                    { plant && (plant.exportTillDate || plant.exportTillDate === 0) ? Utils.round(plant.exportTillDate/1000, 2) : 'NA'}
                                </h6>
                                <span className="d-block f-10">4G</span>
                            </Col>
                            <Col sm={6} className="text-center my-2" style={{ padding: '0 10px'}}>
                                <h6 className="f-w-300 m-0 mb-1" style={{ letterSpacing: '1px'}}>
                                    { plant && (plant.acCapacity || plant.acCapacity === 0 ) ? Utils.round(plant.acCapacity/1000, 2) : 'NA'} MW
                                </h6>
                                <span className="d-block f-10">2G</span>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer style={{ padding: '15px' }}>
                        <div className={`d-flex align-items-center flex-wrap ${ type !== "Popover" ? 'justify-content-between' : 'justify-content-center' }`}>
                            <Badge pill variant="secondary" className={ type === "Popover" ? "m-1" : null } style={{ padding: '6px 10px', lineHeight: '0.9' }}>
                                Deviation : { plant && (plant.PRDeviation || plant.PRDeviation === 0 ) ? Utils.round(plant.PRDeviation, 2) : 'NA'} %
                            </Badge>
                            <Badge pill variant="secondary" className={ type === "Popover" ? "m-1" : null } style={{ padding: '6px 10px', lineHeight: '0.9' }}>
                                <i className="fa fa-map-marker mr-1"></i>
                                { plant.state }
                            </Badge>
                        </div>
                    </Card.Footer>
                </Card>
            </a>
        );
    }
}

export default PlantToolTip;