import React, { Component } from 'react';
import { Popover, Table, OverlayTrigger } from 'react-bootstrap';


const PopoverContent = (
    <Table className="m-0 plant-status-table" bordered>
        <thead>
            <tr>
                <th className="text-center bg-dark text-light">Colour</th>
                <th className="text-center bg-dark text-light">Deviation(%)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><div className="clr-box clr-box-1"></div></td>
                <td className="text-center"> { "< -5%" }</td>
            </tr>
            <tr>
                <td><div className="clr-box clr-box-2"></div></td>
                <td className="text-center">-2 to -5%</td>
            </tr>
            <tr>
                <td><div className="clr-box clr-box-3"></div></td>
                <td className="text-center">{ ">-2%" }</td>
            </tr>
            <tr>
                <td><div className="clr-box clr-box-4"></div></td>
                <td className="text-center">Low Radiation</td>
            </tr>
            <tr>
                <td><div className="clr-box clr-box-5"></div></td>
                <td className="text-center">No recent communication</td>
            </tr>
            <tr>
                <td><div className="clr-box clr-box-6"></div></td>
                <td className="text-center">Insufficient Data for PR</td>
            </tr>
        </tbody>
    </Table>
);


class PlantLegend extends Component {
    constructor() {
        super();
        this.overlay = React.createRef();
    }

    _mouseEnter() {
        this.overlay.current.handleShow();
    }
    _mouseLeave() {
        this.overlay.current.handleHide();
    }

    render() {
        return (
            <OverlayTrigger ref={this.overlay} trigger="hover" rootClose={true} placement="left" overlay={
                <Popover onMouseLeave={this._mouseLeave.bind(this)} onMouseEnter={this._mouseEnter.bind(this)} id="popover-positioned-left" style={{ maxWidth: "100%" }} className="table-tooltip shadow-lg rounded-sm">
                   { PopoverContent }
                </Popover> }
            >
                <span className="float-right f-16">
                    <i className="fa fa-info-circle" style={{color: '#999999'}} />
                </span>
            </OverlayTrigger>
        );
    }
}

export default PlantLegend;