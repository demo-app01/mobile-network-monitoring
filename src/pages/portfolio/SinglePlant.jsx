import React, { Component } from 'react';
import { Popover, OverlayTrigger } from 'react-bootstrap';
import PlantToolTip from './PlantToolTip';

class SinglePlant extends Component {
    constructor() {
        super();
        this.overlay = React.createRef();
    }

    _mouseEnter() {
        this.overlay.current.handleShow();
    }
    _mouseLeave() {
        this.overlay.current.handleHide();

    }

    render() {
        const { plant } = this.props;
        return (
            <OverlayTrigger trigger="hover" ref={this.overlay} rootClose={true} placement="top" overlay={
                <Popover onMouseLeave={this._mouseLeave.bind(this)} onMouseEnter={this._mouseEnter.bind(this)} id="popover-positioned-top" className="shadow-lg plant-tooltip rounded-sm">
                    <PlantToolTip type="Popover" plant={plant}/>
                </Popover>} >
                <div className={`wrapper_t ${plant.statusColor ? plant.statusColor : 'default'}`} >
                    <a className="outer_div_link" href={"/"+plant.plantName+"/dashboard"}>
                        <span style={{ opacity: 0.5 }}>.</span>
                    </a>
                </div>
            </OverlayTrigger>
        );
    }
}
        
export default SinglePlant;