import React, { Component } from 'react';
import Select from 'react-select';
import { Row, Col, Card, Button, Modal } from 'react-bootstrap';
import axios from 'axios';
import ReactDOMServer from "react-dom/server";
import { ProgressBar } from 'react-bootstrap';
import PlantGridList from './PlantGridList';
import PlantToolTip from './PlantToolTip';
import Chart from '../Alerts/Chart'
import Spinner from '../../App/Utils/Spinner';
import Utils from '../../Utils';
import _ from 'lodash';
// import SitesTreeView from '../TreeView/TreeView'
import PortfolioCard from './PortfolioCard'
import moment from 'moment'
import PanelwithName from '../ReusableComponents/PanelwithName';
import PlantLegend from './PlantLegend';
import Highcharts from 'highcharts';
//Redux dependencies


import MapIconGreen from '../../assets/images/map_icons/MapIconGreen.png'
import MapIconRed from '../../assets/images/map_icons/MapIconRed.png'
import MapIconYellow from '../../assets/images/map_icons/MapIconYellow.png'
import MapIconBlack from '../../assets/images/map_icons/MapIconBlack.png'
import MapIconGray from '../../assets/images/map_icons/MapIconGray.png'
import MapIconOrange from '../../assets/images/map_icons/MapIconOrange.png'
import plantImage from '../../assets/images/solar/plant.png'
import L from 'leaflet'
import "overlapping-marker-spiderfier-leaflet/dist/oms";
import "leaflet/dist/leaflet.css";
import 'leaflet.fullscreen/Control.FullScreen';
import 'leaflet.fullscreen/Control.FullScreen.css';
import screenfull from 'screenfull';
window.screenfull = screenfull;
const OverlappingMarkerSpiderfier = window.OverlappingMarkerSpiderfier;

let map = null;

function LeafletTooltip({ plantData, plant }) {
    return (
        <div className="tooltip-wrapper">
            <div style={{ height: 100 }} className="position-relative border">
                <div style={{ height: 50, whiteSpace: "nowrap" }} className={`d-block position-relative ${plant.plantStatusColor}`}>
                    <div style={{ left: 95, bottom: 4, right: 5 }} className="position-absolute d-flex align-items-center justify-content-between">
                        <a href={`/${plantData.plantName}/dashboard`} className="f-14" style={{ color: "#FFFFFF" }} title="View Plant Data">
                            { plantData.plantDisplayName }
                        </a>
                        <a target="_blank" href={`http://maps.google.com/?q=${plantData.latitude},${plantData.longitude}&z=16`} style={{ padding: "2px 6px", borderRadius: 2 }} className="f-10 bg-white" title="View Plant Location">
                            View on Google Map
                        </a>
                    </div>
                </div>
                <div className="position-absolute" style={{ left: 95, right: 5, bottom: "5%" }}>
                    <div className="d-flex justify-content-between mb-1">
                        <span className="f-12">Plant Capacity : {plant.plantCapacity} MW</span>
                        <span className="f-12">PR Deviation : {plant.PRDeviation}</span>
                    </div>
                    <span className="f-10">
                        <i className="fa fa-map-marker mr-1" />
                        { plantData.state }
                    </span>
                </div>
                <div className="position-absolute" style={{ top: "50%", left: 10, transform: "translateY(-50%)" }}>
                    <img style={{ width: 80, height: 80, border: "2px solid #FFFFFF" }} className="plant-logo rounded-circle" src={plantImage} alt="Plant Logo" />
                </div>
            </div>
            <div className="d-flex align-items-center border-left border-right border-bottom">
                <Col sm={3} className="border-right text-center" style={{ fontSize: 9, padding: "12.5px 0" }}>
                    <span className="f-14" style={{ color: "#0eaaa6" }}>{ plant.pr}</span><br />
                    PR
                </Col>
                <Col sm={3} className="border-right text-center py-2" style={{ fontSize: 9 }}>
                    <span className="f-12">{ plant.insolationPOA}</span><br />
                    Insolation (kWh/m2)
                </Col>
                <Col sm={3} className="border-right text-center py-2" style={{ fontSize: 9 }}>
                    <span className="f-12">{ plant.exportToday}</span><br />
                    Today's Energy (MWh)
                </Col>
                <Col sm={3} className="text-center py-2" style={{ fontSize: 9 }}>
                    <span className="f-12">{ plant.exportTillDate }</span><br />
                    Total Energy (MWh)
                </Col>
            </div>
        </div>
    );
}

let sites = require('./sites.json');


class Portfolio extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedCountries: '',
            selectedRegions: '',
            selectedSites: '',
            data: null,
            plantsPRData:null,
            portfolioStatus: null,
            chartConfig: null,
            availibilty: null,
            overAllPR:null,
            plantAvailibiltyChart: {},
            gridAvailibiltyChart: {},
            isMapView: true,
            elementList: null,
            plantsList: sites,
            countryOptions: [{}],
            stateOptions: [{}],
            plants: [{}],
            showPlantAvailability: false,
            showGridAvailability: false,
            toaster:false,
            customName: (<span>
                Prevented CO<sub>2</sub> in Tonnes
            </span>)

        }
        this.handleClick = this.handleClick.bind(this);
        this.buttonClick = this.buttonClick.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.closePlantAvailibility = this.closePlantAvailibility.bind(this);
        this.closeGridAvailibility = this.closeGridAvailibility.bind(this);


    }
    
    componentDidMount() {

        // this.props.onPortfolioEnter();
        this.reloadSites()
        this.interval = setInterval(() => {
             this.reloadSites()
            //  this.loadData(() => {
            //     this.parseData();
            //     this.plotAvailibiltyGraph();
            //     this.plantSelection()
                this.initMap();
            // })
        }, 60000);
    }

    componentWillUnmount() {
        // this.props.onPortfolioExit();
        clearInterval(this.interval);
    }

    buttonClick() {
        this.setState(prevState => ({
            isMapView: !prevState.isMapView
        }));
    }
    

    initMap() {
        const latitude = process.env.REACT_APP_MAP_LATITUDE?process.env.REACT_APP_MAP_LATITUDE:20.5937
        const longitude = process.env.REACT_APP_MAP_LONGITUDE?process.env.REACT_APP_MAP_LONGITUDE:78.9629
        const zoomLevel = process.env.REACT_APP_MAP_ZOOM_LEVEL?process.env.REACT_APP_MAP_ZOOM_LEVEL:5
        var self = this
        const satellite =  L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}');
        var mbAttr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
					'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
					'Imagery © <a href="http://mapbox.com">Mapbox</a>',
				mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
        var streetsMapbox   = L.tileLayer(mbUrl, {id: 'mapbox.streets', attribution: mbAttr})

        var  ImageryClarity =  'http://clarity.maptiles.arcgis.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
             attribution='<a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a> | Powered by <a href="https://www.esri.com">Esri</a>'
        var ImageryClarity   = L.tileLayer(ImageryClarity, {attribution: attribution,attributionUrl: 'https://static.arcgis.com/attribution/World_Imagery'})

        let layerLabels = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}');

        if (map){
            map.remove()
        };
        map = L.map('map', {
            center:[latitude, longitude],
            zoom: zoomLevel,
            maxZoom:16,
            scrollWheelZoom:false,
            fullscreenControl: true,
            fullscreenControlOptions: {
              position: 'topleft'
            },
            layers: [ImageryClarity,layerLabels]

        });
        var baseLayers = {
            "Satellite": satellite,
            'Streets':streetsMapbox,
            'Imagery (Clarity)':ImageryClarity,
            'Imagery (Clarity)':layerLabels,
        };
        L.control.layers(baseLayers).addTo(map);
        const oms = new OverlappingMarkerSpiderfier(map, { keepSpiderfied: true });
        
        updateMarkers()
        
        oms.markers.forEach(marker => marker.setIcon(createIcon(marker.options.statusColor)));
        oms.circleFootSeparation = 50;

        function createIcon(plantColorStatus) {
            let mapIcon
            switch (plantColorStatus) {
                case "stage_1":
                    mapIcon = MapIconGray;
                    break;
                case "stage_3":
                    mapIcon = MapIconRed;
                    break;
                case "stage_4":
                    mapIcon = MapIconYellow;
                    break;
                case "stage_5":
                    mapIcon = MapIconOrange;
                    break;
                case "stage_9":
                    mapIcon = MapIconBlack;
                    break;
                case "blackOutline":
                    mapIcon = MapIconBlack;
                    break;
                case "stage_2":
                    mapIcon = MapIconGreen;
                    break;
                default:
                    mapIcon = MapIconBlack;
            }

            return new L.Icon({
                iconUrl: mapIcon,
                popupAnchor: [1, -34],
                className: 'mapIcon bounce',
                shadowUrl: 'marker-shadow.png',
            });
        }

        function updateMarkers(){
            for(var i = 0; i < oms.getMarkers().length; i++){
                map.removeLayer(oms.getMarkers()[i]);
            }
            oms.unspiderfy();
            let plants = self.state.elementList 
            console.log(self.state.elementList)
            Object.keys(plants).map(function(siteData,i,pr,PRDeviation,insolationPOA,exportToday,exportTillDate,plantCapacity,plantStatusColor){
                pr = (plants[siteData].PR ||plants[siteData].PR === 0) ? Utils.round(plants[siteData].PR,2) + '%' : 'NA'
                PRDeviation = (plants[siteData].PRDeviation || plants[siteData].PRDeviation === 0) ? Utils.round(plants[siteData].PRDeviation,2) + '%' : 'NA'
                insolationPOA = (plants[siteData].insolationPOA || plants[siteData].insolationPOA === 0 )? Utils.round(plants[siteData].insolationPOA,2) : 'NA'
                exportToday = (plants[siteData].exportToday || plants[siteData].exportToday === 0 ) ? Utils.round(plants[siteData].exportToday/1000,2) : 'NA'
                exportTillDate = (plants[siteData].exportTillDate || plants[siteData].exportTillDate === 0) ? Utils.round(plants[siteData].exportTillDate/1000,2) : 'NA'
                plantCapacity = plants[siteData].acCapacity ? Utils.round(plants[siteData].acCapacity /1000,2) : 'NA'
                plantStatusColor = plants[siteData].statusColor ? plants[siteData].statusColor : 'default'
                        
               return L.marker([plants[siteData].latitude, plants[siteData].longitude], plants[siteData])
                .bindPopup(ReactDOMServer.renderToString(<LeafletTooltip plantData={plants[siteData]} plant={{ pr: pr, PRDeviation: PRDeviation, insolationPOA: insolationPOA, exportToday: exportToday, exportTillDate: exportTillDate, plantCapacity: plantCapacity, plantStatusColor: plantStatusColor }} />))
                }).forEach(marker => {
                    map.addLayer(marker);
                    oms.addMarker(marker);
                    
                }            
            );
            oms.markers.forEach(marker => marker.setIcon(createIcon(marker.options.statusColor)));
        }
        setInterval(updateMarkers, 60000);

    }

    reloadSites(){
        let sitelist = {};
        let ids ='';
        const { plantsList, countries, regions, sites,} = this.state
        
        if (sites) {
            ids = sites;
        } else if (regions) {   
            let selectedPlants = plantsList.filter(plant => {
                if(regions.includes(plant.state)){
                    return plant;
                }
            });
            ids = selectedPlants.map(plant => plant.siteId.toString());
        } else if (countries) {
            let selectedPlants = plantsList.filter(plant => {
                if(countries.includes(plant.country)){
                    return plant;
                }
            });
            ids = selectedPlants.map(plant => plant.siteId.toString());
        } else {
            ids = plantsList.map(plant => plant.siteId.toString());
        }
        plantsList.forEach((plant)=>{
            let plantId = plant.siteId
                plant["plantId"] = plantId 
        })
        let sortedData = plantsList.sort((a,b) => (a.plantDisplayName > b.plantDisplayName) ? 1 : ((b.plantDisplayName > a.plantDisplayName) ? -1 : 0));
        sortedData.forEach((plant)=>{
            if(ids && ids.includes(plant.siteId.toString())){
                sitelist[plant.siteId+'_Plant'] = plant
            }
        })
        this.setState({
            elementList: sitelist
        },()=>{
            console.log(this.state.elementList)
                this.plotAvailibiltyGraph();
            // this.loadData(() => {
            //     this.parseData();
            //     this.plantSelection();
            // });
            this.initMap();
        });
    }

    loadData(callback) {
        const { elementList, countries, regions, sites, plantsList } = this.state
        if(!elementList){
            return;
        }
        let ids = '';
        if (sites) {
            ids = sites;
        } else if (regions) {   
            let selectedPlants = plantsList.filter(plant => {
                if(regions.includes(plant.state)){
                    return plant;
                }
            });
            ids = selectedPlants.map(plant => plant.siteId).toString();
        } else if (countries) {
            let selectedPlants = plantsList.filter(plant => {
                if(countries.includes(plant.country)){
                    return plant;
                }
            });
            ids = selectedPlants.map(plant => plant.siteId).toString();
        } else {
            ids = plantsList.map(plant => plant.siteId).toString();
        }
        let to = moment(new Date()).toDate();
        let from = moment(new Date()).subtract(6,'d').toDate();
        let plantsPRAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/portfolio/kpi?siteIds='+ids+'&dateFrom='+Utils.getLocalYYYYMMDD(from)+'&dateto='+Utils.getLocalYYYYMMDD(to)
        let plantsKpiAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/portfolio/kpi?siteIds='+ids
        let monthGenerationAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/portfolio/generation?siteIds='+ ids
        let estimatedPlantKpiApi = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/portfolio/estimated-kpi?siteIds='+ ids+'&summary=day,plant'
        axios.all([axios.get(plantsKpiAPI),axios.get(monthGenerationAPI),axios.get(plantsPRAPI),axios.get(estimatedPlantKpiApi)], { withCredentials: true })
        .then(axios.spread((plantsKpi,generation,plantsPRData,estimatedPlantKpi) => {

            let sumPR = 0;
            let countPR = 0;
            plantsKpi.data.forEach((el)=>{
                if (el.PR || el.PR === 0 ) {
                    sumPR += el.PR;
                    countPR++;
                }
            })
            let avgPR = sumPR / countPR;

            let siteIds = ''
            siteIds = plantsKpi.data.map(plant => plant.device.split('_')[0])
            let portfolioStatus = { totalExportToday : 0, totalExportTillDate: 0, acSumCapacity:0, dcSumCapacity:0}
                plantsKpi.data.forEach((plant)=>{
                        if (elementList[plant.device]) {
                            portfolioStatus.totalExportTillDate += plant.exportTillDate ? plant.exportTillDate : 0
                            portfolioStatus.totalExportToday += plant.exportToday ? plant.exportToday : 0
                            portfolioStatus.acSumCapacity += elementList[plant.device].acCapacity
                            portfolioStatus.dcSumCapacity += elementList[plant.device].dcCapacity;
                            elementList[plant.device]._id = plant._id
                            elementList[plant.device].date = plant.date
                            elementList[plant.device].device = plant.device
                            elementList[plant.device].insolationPOA = plant.insolationPOA
                            elementList[plant.device].pa = plant.pa
                            elementList[plant.device].ACCUF = plant.ACCUF
                            elementList[plant.device].DCCUF = plant.DCCUF
                            elementList[plant.device].PR = plant.PR
                            elementList[plant.device].exportTillDate = plant.exportTillDate
                            elementList[plant.device].exportToday = plant.exportToday
                            elementList[plant.device].importTillDate = plant.importTillDate
                            elementList[plant.device].importToday = plant.importToday
                            elementList[plant.device].lcts = plant.lcts
                            elementList[plant.device].maxAC = plant.maxAC
                            elementList[plant.device].yield = plant.yield
                            let deviation = estimatedPlantKpi.data.find(el => el.device === plant.device) ? (plant.PR- estimatedPlantKpi.data.find(el => el.device === plant.device).PR ): ((plant.PR-avgPR)/avgPR)*100
                            let commTimeoutMills = elementList[plant.device].commTimeoutMins ? Math.floor(elementList[plant.device].commTimeoutMins * 60 * 1000) : Math.floor(10 * 60 * 1000)
                            
                            if((Date.now() - new Date(plant.lcts)) >= commTimeoutMills){
                                elementList[plant.device].statusColor = 'stage_9'
                            }
                            else if(plant.lcts && plant.sunlight && ((new Date(plant.lcts)-new Date(plant.sunlight.endTs)) > commTimeoutMills)){
                                elementList[plant.device].statusColor = 'stage_1'
                            }
                            else if(deviation >= -5 && deviation <= -2){
                                elementList[plant.device].statusColor = 'stage_4'
                            }else if(deviation > -2){
                                elementList[plant.device].statusColor = 'stage_2'
                            }else if(deviation < -5 ){
                                elementList[plant.device].statusColor = 'stage_5'
                            }else{
                                elementList[plant.device].statusColor = 'blackOutline'
                            }
                            elementList[plant.device].PRDeviation = deviation
                        }
                })
            portfolioStatus.preventedCO2 = portfolioStatus.totalExportTillDate * 0.000707
            portfolioStatus.monthGeneration = generation.data.monthGeneration
            this.setState({
                elementList: elementList,
                siteIds: siteIds,
                portfolioStatus: portfolioStatus,
                plantsPRData: plantsPRData.data
            })
            this.props.plantDataChange(this.state.portfolioStatus.acSumCapacity,this.state.portfolioStatus.dcSumCapacity)
            callback();
        }))
        .catch(err => {
            this.setState({
                // elementList: {},
                portfolioStatus:{},
                plantsPRData:[],
                toaster:true,
                toastType:'error',
                title:'Network Error',
                text:'Error While Retrieving Portfolio kpis Data',

            })
            callback();
        })
    }

    parseData() {
        let { plantsPRData } = this.state;
        let categories = [];
        plantsPRData.forEach((plant)=>{
            if(!categories.includes(moment(plant.date).format('dddd'))){
                categories.push(moment(plant.date).format('dddd'))
            }
        })
        let groupedPlantData =_.groupBy(plantsPRData,(el=>el.device))
        let sData = [];
        for(let plant in groupedPlantData){
            let data =[];
            groupedPlantData[plant].forEach((el)=>{
                if(el.PR || el.PR === 0){
                    data.push({'x':new Date((el.date).substring(0, 16) + "Z").getTime(),'y':el.PR})
                }
            })
            if (data.length > 0) {
                let plantId = parseInt(plant.split('_')[0])
                let temp = {
                    type: 'line',
                    name: Utils.getSiteFromSiteId(plantId)["plantDisplayName"],
                    data: data,
                    tooltip: { valueSuffix: ' %' },
                }
                sData.push(temp)
            }
        }
        let cConfig = {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Performance Ratio'
            },
            xAxis: {
                type: 'datetime',
                labels: {
                    formatter: function() {
                      return Highcharts.dateFormat('%A', this.value);
                    }
                  }
            },
            yAxis: {
                title: {
                    text: 'PR'
                },
                min:0,
                max: 100
            },
            exporting: {
                sourceWidth: window.innerWidth,
                sourceHeight: 500,
                scale: 1,
                filename: 'Performance Ratio',
                chartOptions: {
                    title: {
                        text: 'Performance Ratio'
                    }
                }
            },
            legend: {
                enabled: true
            },
            lang: {
                noData: "No data to display"
            },
            tooltip: {
                useHTML: true,
                shared: true,
                crosshairs: true,
            },
            series: sData
        }
        this.setState({
            chartConfig: cConfig
        })
    }

    plotAvailibiltyGraph() {

        const { siteIds } = this.state;
        let from = moment().startOf('month').toDate();
        let to = moment().startOf('end').toDate();
        let data = [
            {
            "PR": 74.26523809523809,
            "pa": 99.5525,
            "ga": 100,
            "device": "2_Plant"
            },
            {
            "PR": 81.24782608695652,
            "pa": 100,
            "ga": 100,
            "device": "1_Plant"
            },
            {
            "PR": 90.7690909090909,
            "pa": 100,
            "ga": 100,
            "device": "3_Plant"
            }
            ]
            let PR = 0;
            data.forEach((site) => { 
                if(site.PR || site.PR === 0){
                    PR += site.PR 
                }
            })
            this.setState({
                availibilty: data,
                overAllPR: PR/data.length
            }, () => {
                this.plotGraph()
            })
        // let availibiltyAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/portfolio/kpi?siteIds=' + siteIds + '&dateFrom='+Utils.getLocalYYYYMMDD(from)+'&dateTo='+Utils.getLocalYYYYMMDD(to)+'&summary=plant';
        // axios.get(availibiltyAPI, { withCredentials: true })
        //     .then(res => {
        //         let PR = 0;
        //         res.data.forEach((site) => { 
        //             if(site.PR || site.PR === 0){
        //                 PR += site.PR 
        //             }
        //         })
        //         this.setState({
        //             availibilty: res.data,
        //             overAllPR: PR/res.data.length
        //         }, () => {
        //             this.plotGraph()
        //         })
        //     })
        //     .catch(err => {
        //         this.setState({
        //             availibilty: [],
        //             toaster:true,
        //             toastType:'error',
        //             title:'Network Error',
        //             text:'Error While Retrieving Portfolio Availibilty Data',
        //         })
        //     })
    }

    plotGraph() {
        const { availibilty } = this.state;
        let seriesData = {};
        let PA = 0;
        let GA = 0;
        let PAPie = []
        let GAPie = []
        let PACount=0
        let GACount=0
        availibilty.forEach((site) => {
            if(site.pa ||site.pa==0){
                PA += site.pa
                PACount++
            }
            if(site.ga || site.ga == 0){
                GA += site.ga
                GACount++
            }
            seriesData["portfolioAvailibilty"] = PA;
            seriesData["gridAvailibilty"] = GA;
        })
        seriesData["portfolioAvailibilty"] = seriesData["portfolioAvailibilty"] / PACount
        seriesData["gridAvailibilty"] = seriesData["gridAvailibilty"] / GACount
        seriesData["portfolioUnavailibilty"] = seriesData.portfolioAvailibilty < 100 ? 100 - Utils.round(seriesData.portfolioAvailibilty, 2) : 0
        seriesData["gridUnavailibilty"] = seriesData.gridAvailibilty < 100 ? 100 - Utils.round(seriesData.gridAvailibilty, 2) : 0

        let portfolioAvailibilty = [];
        let gridAvailibilty = [];
        let siteNames = ["Qatar",
        "Qatar 2",
        "Qatar 3"];
        availibilty.forEach((site, index) => {
            let plantDisplayName = '' //Utils.getSiteFromSiteId(parseInt(site.device.split('_')[0]))["plantDisplayName"]
            let plantUrlName = '' //Utils.getSiteFromSiteId(parseInt(site.device.split('_')[0]))["plantName"]
            // siteNames.push(Utils.getSiteFromSiteId(parseInt(site.device.split('_')[0]))["plantDisplayName"])
            portfolioAvailibilty.push({
                'name': plantDisplayName,
                'y': Utils.round(site.pa,2),
                url: '/' + plantUrlName + '/dashboard'
            })
            gridAvailibilty.push({
                'name': plantDisplayName,
                'y': Utils.round(site.ga,2),
                url: '/' + plantUrlName + '/dashboard'
            })
        })
        let paBarChart = {
            chart: {
                type: 'bar'
            },
            legend: {
                enabled: false
            },
            title: {
                text: 'Plant Availability'
            },
            xAxis: [{
                categories: siteNames,
                crosshair: true
            }],
            yAxis: {
                min: 0,
                max: 100,
            },
            exporting: {
                sourceWidth: window.innerWidth,
                sourceHeight: 500,
                scale: 1,
                filename: 'plant Availability',
                chartOptions: {
                    title: {
                        text: 'plant Availability'
                    }
                }
            },
            plotOptions: {
                series: {
                    point: {
                        events: {
                            click: function () {
                                if (window.screen.width <= '450' && window.screen.height <= '850') {
                                    window.open(this.options.url, '_self');
                                } else {
                                    window.open(this.options.url, '_blank');
                                }
                            }
                        }
                    }
                }
            },
            lang: {
                noData: "No data to display"
            },
            series: [{
                name: 'Plant Availability',
                colorByPoint: true,
                click: this.openPlantAvailibility,
                data: portfolioAvailibilty,
                tooltip: {
                    valueSuffix: ' %'
                },
            }]
        }
        this.setState({
            paBarChartConfig: paBarChart
        })

        let gaBarChart = {
            chart: {
                type: 'bar'
            },
            legend: {
                enabled: false
            },
            title: {
                text: 'Grid Availability'
            },
            xAxis: [{
                categories: siteNames,
                crosshair: true
            }],
            yAxis: {
                min: 0,
                max: 100,
            },
            exporting: {
                sourceWidth: window.innerWidth,
                sourceHeight: 500,
                scale: 1,
                filename: 'Grid Availability',
                chartOptions: {
                    title: {
                        text: 'Grid Availability'
                    }
                }
            },
            plotOptions: {
                series: {
                    point: {
                        events: {
                            click: function () {
                                if (window.screen.width <= '450' && window.screen.height <= '850') {
                                    window.open(this.options.url, '_self');
                                } else {
                                    window.open(this.options.url, '_blank');
                                }
                            }
                        }
                    }
                }
            },
            lang: {
                noData: "No data to display"
            },
            series: [{
                name: 'Grid Availability',
                colorByPoint: true,
                click: this.openGridAvailibility,
                data: gridAvailibilty,
                tooltip: {
                    valueSuffix: ' %'
                },
            }]
        }
        this.setState({
            gaBarChartConfig: gaBarChart
        })

        if (availibilty.length > 0) {

            PAPie = [

                {
                    name: 'Plant',
                    data: [{
                        name: 'Available',
                        y: Utils.round(seriesData.portfolioAvailibilty, 2),
                        id: seriesData.portfolioAvailibilty,
                        legendIndex:0,
                        color : {
                        linearGradient: { },
                        stops: [
                            [0,'#1de9b6'],
                            [1,Highcharts.Color('#1de9b6').brighten(-0.2).get('rgb')]  
                        ]}
                    },
                    {
                        name: 'Unavailable',
                        y: Utils.round(seriesData.portfolioUnavailibilty, 2),
                        id: seriesData.portfolioUnavailibilty,
                        legendIndex:1,
                        color : {
                            linearGradient: { },
                            stops: [
                                [0,'#f44236'],
                                [1,Highcharts.Color('#f44236').brighten(-0.2).get('rgb')]  
                            ]} 
                    }]
                    ,
                    tooltip: {
                        valueSuffix: ' %'
                    },
                }
            ]


            GAPie = [
                {
                    name: 'Grid',
                    data: [
                        {
                            name: 'Available',
                            y: seriesData.gridAvailibilty,
                            id: seriesData.gridAvailibilty,
                            legendIndex:0,
                            color : {
                            linearGradient: { },
                            stops: [
                                [0,'#1de9b6'],
                                [1,Highcharts.Color('#1de9b6').brighten(-0.2).get('rgb')]  
                            ]}  
                        },
                        {
                            name: 'Unavailable',
                            y: seriesData.gridUnavailibilty,
                            id: seriesData.gridUnavailibilty,
                            legendIndex:1,
                            color : {
                                linearGradient: { },
                                stops: [
                                    [0,'#f44236'],
                                    [1,Highcharts.Color('#f44236').brighten(-0.2).get('rgb')]  
                                ]} 
                        }],
                    tooltip: {
                        valueSuffix: ' %'
                    },
                }


            ]
        }

        let newThis = this
        let paChart = {
            chart: {
                type: 'pie'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: '5G Availability'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage} %</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false
                    },
                    borderWidth: 0
                },
                series: {
                    point: {
                        events: {
                            click: function () {
                                newThis.setState({
                                    showPlantAvailability: true
                                })
                                function onClick(e) {
                                    if (window.screen.width <= '450' && window.screen.height <= '850') {
                                        window.open(newThis.options.url, '_self');
                                    } else {
                                        window.open(newThis.options.url, '_blank');
                                    }

                                };
                            }
                        }
                    }
                }
            },
            lang: {
                noData: "No data to display"
            },
            series: PAPie
            ,
            tooltip: {
                headerFormat: 'Portfolio Availability<br>',
                pointFormat: '<span style="color:{series.color}">●</span><b>{point.name}: </b> <b>{point.y}<b><br/>'
            }
        }

        let gaChart = {
            chart: {
                type: 'pie'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: '4G & Others Availability'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage} %</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false
                    },
                    borderWidth: 0
                },
                series: {
                    point: {
                        events: {
                            click: function () {
                                newThis.setState({
                                    showGridAvailability: true
                                })
                                function onClick(e) {
                                    if (window.screen.width <= '450' && window.screen.height <= '850') {
                                        window.open(newThis.options.url, '_self');
                                    } else {
                                        window.open(newThis.options.url, '_blank');
                                    }

                                };
                            }
                        }
                    }
                }
            },
            lang: {
                noData: "No data to display"
            },
            series: GAPie,
            tooltip: {
                headerFormat: 'Grid Availability<br>',
                pointFormat: '<span style="color:{series.color}">●</span><b>{point.name}: </b> <b>{point.y}<b><br/>'
            }
        }
        
        this.setState({
            plantAvailibiltyChart: paChart,
            gridAvailibiltyChart: gaChart
        })
    }

    handleClick(plant) {
        // this.props.history.push({pathname:'/'+plant+'/dashboard'})
    }

    handleCountryChange = selectedCountries => {
        this.setState({ selectedCountries });
    }
    handleRegionChange = selectedRegions => {
        let plantList = this.state.plantsList;
        this.setState({ selectedRegions });
        let filteredsites, plants = [];
        filteredsites = plantList.filter((plant) => {
            let site;
            if (selectedRegions) {
                selectedRegions.forEach((region) => {
                    if (plant.state === region.value) {
                        site = plant;
                    } else {
                        return;
                    }
                })
            }
            return site;
        })
        filteredsites && filteredsites.map((element, index) =>
            plants.push({ 'value': element.siteId, 'label': element.plantDisplayName })
        )
        if (filteredsites.length > 0) {
            this.setState({
                plants: plants
            })
        } else {
            this.plantSelection()
        }

    }
    handlesitechange = selectedSites => {
        this.setState({ selectedSites });
    }
    onSubmit() {
        const { selectedRegions, selectedCountries, selectedSites } = this.state;
        let countries = '', regions = '', sites = '';
        if (selectedCountries) {
            countries = selectedCountries.map(country => country.value.toString())
        }
        if (selectedRegions) {
            regions = selectedRegions.map(region => region.value.toString())
        }
        if (selectedSites) {
            sites = selectedSites.map(site => site.value.toString())
        }
        
        this.setState({
            countries: countries,
            regions: regions,
            sites: sites,
            selectedCountries: '',
            selectedRegions: '',
            selectedSites: '',
            data: null,
            portfolioStatus:null,
            availibilty: null,
            elementList:null
        }, () => {
            this.reloadSites()
            // this.loadData(() => {
            //     this.parseData();
            //     this.plotAvailibiltyGraph();
            //     this.plantSelection()
            // });
            this.initMap();
        })

    }


    plantSelection() {
        var country = []
        var states = []
        var plants = []
        var plantDetails = this.state.plantsList
        plantDetails.forEach(function (plant) {
            country.push(plant.country)
            states.push(plant.state)
        });
        plantDetails.map((element, index) =>
            plants.push({ 'value': element.siteId, 'label': element.plantDisplayName })
        )
        const filteredcountries = country.reduce((x, y) => x.includes(y) ? x : [...x, y], []);
        const filteredstates = states.reduce((x, y) => x.includes(y) ? x : [...x, y], []);

        let countryOptions = [], stateOptions = [];
        filteredcountries && filteredcountries.map((element, index) =>
            countryOptions.push({ 'value': element, 'label': element })
        )
        filteredstates && filteredstates.map((element, index) =>
            stateOptions.push({ 'value': element, 'label': element })
        )
        this.setState({
            countryOptions: countryOptions,
            stateOptions: stateOptions,
            plants: plants
        })
    }

    closePlantAvailibility() {
        this.setState({
            showPlantAvailability: false
        });
    }

    openPlantAvailibility() {
        this.setState({
            showPlantAvailability: false
        });
    }

    closeGridAvailibility() {
        this.setState({
            showGridAvailability: false
        });
    }

    openGridAvailibility() {
        this.setState({
            showGridAvailability: false
        });
    }

    render() {
        const { elementList, stateOptions, countryOptions, plants, availibilty, chartConfig, plantAvailibiltyChart, gridAvailibiltyChart, paBarChartConfig, gaBarChartConfig, isMapView, customName, portfolioStatus, overAllPR,toaster,toastType,title,text } = this.state;
        return (
            <React.Fragment>
                <Row className="mt-4 portfolioHeader">
                    <PortfolioCard value='10,000,056 Users' color="blue" icon="fa-dashboard" name="Total no. of users" unit='' />
                    <PortfolioCard value='150,410 5G' color="green" icon="fa-calendar" name="Users in 5G" unit=''/>
                    <PortfolioCard value='1,066,282 4G' color="red" icon="fa-bar-chart-o" name="Users in 4G" unit=''/>
                    <PortfolioCard value='176,753,132 Others' color="purple" icon="fa-globe" name="Users in 3G/2G/E" unit='' />
                </Row>
                <hr />
                <Row className="align-items-center justify-content-center filter-row">
                    <Col md="auto" className="my-2">
                        <Button variant="" variant={ isMapView ? "dark" : "outline-dark"} className={`btn btn-sm m-0 mr-1 ${isMapView && "active"}`} onClick={this.buttonClick}>Map View</Button>
                        <Button variant={ isMapView ? "outline-dark" : "dark"} className={`btn btn-sm m-0 ml-1 ${!isMapView && "active"}`}  onClick={this.buttonClick}>Tree View</Button>
                    </Col>
                    <Col xl={2} md={4} className="my-2">
                        <Select
                            onChange={this.handleCountryChange}
                            options={countryOptions}
                            name="country"
                            placeholder="Select Country"
                            isMulti
                            value={this.state.selectedCountries}
                        />
                    </Col>
                    <Col xl={2} md={4} className="my-2">
                        <Select
                            onChange={this.handleRegionChange}
                            options={stateOptions}
                            name="region"
                            placeholder="Select Region"
                            isMulti
                            value={this.state.selectedRegions}
                        />
                    </Col>
                    <Col xl={3} md={5} className="my-2">
                        <Select
                            onChange={this.handlesitechange}
                            options={plants}
                            name="plant"
                            placeholder="Select Plant"
                            isMulti
                            value={this.state.selectedSites}
                        />
                    </Col>
                    <Col xl={2} md={3} className="my-2">
                        <Button variant="success" size="sm" block className="m-0" onClick={this.onSubmit}>Submit</Button>
                    </Col>
                </Row>

                <hr />
                <Row>
                    <Col md={12} xl={6}>
                        <Card className="m-0" style={{'height':'100%'}}>
                            <Card.Body className="p-2">
                                { !elementList ? 
                                <div className="text-center">
                                    <Spinner />
                                </div> : 
                                <React.Fragment>
                                    <div className={ !isMapView ? 'd-none' : 'leaflet-container leaflet-fade-anim leaflet-grab leaflet-touch-drag'}  style={{ width: "100%", height: "620px", float: "left", margin: "0 auto" }} id="map"></div>
                                    { !isMapView ? 
                                    <div></div>
                                    // <SitesTreeView plants={elementList} /> 
                                    : null }
                                </React.Fragment> }
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={12} xl={6}>
                        <Col md={12} className="p-0">
                            <PanelwithName name={"Heat map for 5G/ 4G/ 3G/ 2G & E "} legend={<PlantLegend/>}>
                                { !elementList ?
                                <div className="text-center">
                                    <Spinner />
                                </div> :
                                <PlantGridList Plants={elementList} elementAction={this.handleClick} /> }
                            </PanelwithName>
                        </Col>
                        <Col md={12} className="p-0">
                            <Card className="m-0">
                                <Card.Header>
                                    <Card.Title as="h5">KPIs</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                    <Row>
                                        <Col xl={6} md={12}>
                                            { !availibilty ? (
                                            <div className="text-center">
                                                <Spinner />
                                            </div>) : 
                                            <Chart chartConfig={plantAvailibiltyChart} isGradient={true} darkenColors={true} style={{ style: { height: "250px" } }} />
                                            }
                                        </Col>
                                        <Col xl={6} md={12}>
                                            { !availibilty ? (
                                            <div className="text-center">
                                                <Spinner />
                                            </div> ) : 
                                            <Chart chartConfig={gridAvailibiltyChart} isGradient={true} darkenColors={true} style={{ style: { height: "250px" } }} />
                                            }
                                        </Col>
                                    </Row>
                                </Card.Body>
                                <Card.Footer>
                                    <div className="d-flex align-items-center">
                                        <Col md={3} className="p-0">
                                            <b>MTD PR</b>
                                        </Col>
                                        <Col md={9} className="p-0">
                                            { true ? <ProgressBar style={{ lineHeight: 1, height: 18 }} variant="success" now={'84.98'} label={`${Utils.round(overAllPR, 2)}%`} /> : null }
                                        </Col>
                                    </div>
                                </Card.Footer>
                            </Card>
                        </Col>
                    </Col>
                </Row>
                {/* <hr /> 
                <Row>
                    <Col md={12}>
                        <Card className="m-0">
                            <Card.Header>
                                <Card.Title as="h5">Performance Ratio</Card.Title>
                            </Card.Header>
                            <Card.Body className="p-1">
                                { !chartConfig ? 
                                <div className="text-center py-4">
                                    <Spinner />
                                </div> : <Chart chartConfig={chartConfig} style={{ style: { height: "300px" } }} tooltipFormat={'%A, %b %d'}></Chart> }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row> */}
                <hr />
                 {/* <Row>
                    { !elementList ? 
                    <Col className="text-center py-2"> 
                        <Spinner /> 
                    </Col> : 
                    ( Object.keys(elementList).map((plant, index) => 
                        <Col key={index} xl={4} md={6} sm={6} className="mb-4">
                            <PlantToolTip plant={elementList[plant]} />
                        </Col>
                    ) )}
                </Row> */}
                {/* 
                 <Modal show={this.state.showPlantAvailability} onHide={this.closePlantAvailibility} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
                    <Modal.Header closeButton>
                        <Modal.Title as="h5">Plant Availibility</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Chart chartConfig={paBarChartConfig} style={{ style: { height: "400px" } }} ></Chart>
                    </Modal.Body>
                </Modal>
                <Modal show={this.state.showGridAvailability} onHide={this.closeGridAvailibility} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
                    <Modal.Header closeButton>
                        <Modal.Title as="h5">Grid Availibility</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Chart chartConfig={gaBarChartConfig} style={{ style: { height: "400px" } }}></Chart>
                    </Modal.Body>
                </Modal> */}
            </React.Fragment>
        );
    }
}

export default Portfolio;