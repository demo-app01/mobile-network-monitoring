import React, { Component } from 'react';
import SmallSpinner from '../SmallSpinner';
import { Col, Card } from 'react-bootstrap';

class PortfolioCard extends Component {
   
    render() {
        const{ value, unit, name, icon, color } = this.props;
        return (
            <Col md={6} xl={3}>
                <Card className={`bg-c-${color} bitcoin-wallet shadow-lg rounded-sm mb-2`}>
                    <Card.Body>
                        <h6 className="text-white mb-2">{ !value ? <SmallSpinner /> : `${value} ${unit}` }</h6>
                        <span className="text-white d-block">{ name }</span>
                        <div className="f-40">
                            <i className={`fa ${icon} icon-${color}`} style={{ right: '12px' }} />
                        </div>
                    </Card.Body>
                </Card>
            </Col>
        );
    }
}

export default PortfolioCard;