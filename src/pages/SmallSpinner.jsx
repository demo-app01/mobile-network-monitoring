
import React from 'react';

export default class SmallSpinner extends React.Component {
  render() {
    return (
      <div className="spinner-grow spinner-grow-sm" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    );
  }
}