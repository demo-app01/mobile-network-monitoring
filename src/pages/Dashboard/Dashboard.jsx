import React, { Component } from 'react';
import Aux from "../../hoc/_Aux";
import {Row, Col, Card,Breadcrumb,Button} from 'react-bootstrap';
import { Link } from "react-router-dom";
import axios from "axios";
import Utils from "../../Utils";
import Highcharts from 'highcharts';
// import Chart from '../alerts/Chart'
import moment from 'moment'
import Spinner from '../Spinner';
import SmallSpinner from '../SmallSpinner';
// import Toaster from '../Toaster/Toaster'
import DateRangePicker from 'react-bootstrap-daterangepicker';
import InverterGridList from '../Inverter/InverterGridList'
import 'bootstrap-daterangepicker/daterangepicker.css';
import PanelwithName from '../ReusableComponents/PanelwithName';
// import InverterLegend from '../inverter/MonitorInverter/InverterLegend'; 
// import {Event} from '../Tracking/index'

const WMSMapping = {
    "w32": "GHI",
    "w21": "POA_1",
    "w22": "POA_2",
    "w41": "POA_3",
    "w10": "Mod_Temp_1",
    "w14": "Mod_Temp_2",
    "w13": "Amb_Temp",
    "w12": "Wind_Speed",
    "w11": "Wind_Direction",
}
const MFMMapping = {
    "m32": "AC Power",
}

let devices = require('./devices.json');
class Dashboard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            startDate: moment(new Date()).toDate(),
            maxDate: moment(new Date()).toDate(),
            acpowerData: null,
            radiationData: null,
            wmsData: null,
            plantKpiData:null,
            monthlyKPIData:null,
            monthlyEstimationKPIData:null,
            mfmData:null,
            activePower:null,
            wmsIrradianceData:null,
            inverterKpiChartConfig: {},
            wmsChartConfig: {},
            acPowerVsRadiationChartConfig: {},
            KPIdata:[],
            payloadData:[],
            elementList:devices,
            toaster:false,
            activeInverters:null
        }
        this.handleCustomDate = this.handleCustomDate.bind(this)
        this.toWMSModule = this.toWMSModule.bind(this);
        this.toMFMModule = this.toMFMModule.bind(this);

    }

    toWMSModule(startDate){

           let Date=Utils.yyyymmdd(startDate);
           let path = "/"+ Utils.getSite()+"/wms/dashboard";
           this.props.history.push({pathname:path,hash:'#'+Date})
       
       }
       
    toMFMModule(startDate){

           let Date=Utils.yyyymmdd(startDate);
           let path = "/"+ Utils.getSite()+"/mfm/dashboard";
           this.props.history.push({pathname:path,hash:'#'+Date})
    
    }



    handleCustomDate(event, picker) {
        this.setState({
            startDate: moment(picker.startDate).toDate(),
            acpowerData: null,
            radiationData: null,
            wmsData: null,
            plantKpiData:null,
            mfmData:null,
            wmsIrradianceData:null,
            KPIdata:[],
            payloadData:[],
            elementList:null,
            inverterKpiChartConfig: {},
            wmsChartConfig:{},
            acPowerVsRadiationChartConfig:{}
        }, () => {
            Event(
                "Dashboard",
                "Load Data",
                "",
                Utils.diffInDates(this.state.startDate)
            )
            let date = Utils.getLocalYYYYMMDD(this.state.startDate)            
            if(this.state.startDate.toDateString() !== new Date().toDateString()){
                clearInterval(this.state.intervalId)
                this.reloadInverters()
                this.loadACpowerIrradiance(date)
                this.fetchKPIData(date)
                this.loadPlantStatus(date)
            }
            if(this.state.startDate.toDateString() === new Date().toDateString()){
                this.reloadInverters()
                this.fetchKPIData(date)
                this.loadPlantStatus(date)
                this.loadACpowerIrradiance(date)
                let intervalId = setInterval(()=> {
                                    this.fetchKPIData(date)
                                    this.loadPlantStatus(date)
                                },60000);
                this.setState({ intervalId: intervalId })
            }        
        })
    }

    inverterClick = (inverter) => {
        let path = '/' + Utils.getSite() + '/inverter/dashboard'
        this.props.history.push({pathname:path,hash:'#'+inverter});
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId);
     }

    componentDidMount() {
        // this.setState({
        //     defaultWMS :Utils.getSiteMetaData(Utils.getSite())["defaultWMS"],
        //     radiationDeviceId:Utils.getSiteMetaData(Utils.getSite())["defaultRadiation"],
        //     defaultWMSSite:Utils.getSiteMetaData(Utils.getSite())["defaultWMSSite"],
        //     OGMFMs:Utils.getSiteMetaData(Utils.getSite())["OGMFMs"],
        //     plantName : Utils.getSiteMetaData(Utils.getSite())["plantDisplayName"]
        // }, () => {
        //     let date = Utils.getLocalYYYYMMDD(this.state.startDate)
        //     // this.loadInverterKPIs(date)
        //     this.reloadInverters()
        //     this.fetchKPIData(date)
        //     this.loadPlantStatus(date)
        //     this.loadACpowerIrradiance(date)
        //     if(this.state.startDate.toDateString() == new Date().toDateString() ){
        //         let intervalId = setInterval(()=> {
        //                             this.fetchKPIData(date)
        //                             this.loadPlantStatus(date)
        //                         },60000);
        //         this.setState({ intervalId: intervalId })
        //     }
        // })
    }

    reloadInverters(){
        let inverterList = {};
        axios.get(process.env.REACT_APP_DATA_API_DOMAIN_PATH+'/api/'+ Utils.getSite()+'/assets?deviceType=Inverter',{ withCredentials: true })
            .then( res =>{
                res.data.forEach((inverter)=>{
                    let inverterId = inverter.device.split("_")[2]*1
                    inverter["inverterId"] = inverterId 
                })
                let sortedData = res.data.sort((a,b) => (a.inverterId > b.inverterId) ? 1 : ((b.inverterId > a.inverterId) ? -1 : 0));
                sortedData.forEach((inverter)=>{
                    inverterList[inverter.device] = inverter
                })
                this.setState({
                    elementList: inverterList
                });
                this.loadData();

            }).catch(err =>{
                this.setState({
                    elementList:[],
                    toaster:true,
                    toastType:'error',
                    title:'Network Error',
                    text:'Error While Retrieving SMB list Data',
                })
        })
    }
    fetchKPIData = (date) => {
        let payloadsAPI, kpiAPI;
        payloadsAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH+'/api/'+Utils.getSite()+'/data/'+date+'?deviceType=Inverter&tags=i15,i21';
        kpiAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH+'/api/'+Utils.getSite()+'/kpi/Inverter?dateFrom='+date+'&dateTo='+date;
        axios.all([axios.get(payloadsAPI),axios.get(kpiAPI)], { withCredentials: true })
        .then(axios.spread((payloads,kpi) => {
            this.setState({
                KPIdata:kpi.data,
                payloadData:payloads.data   
            })
            this.loadData();
        }))
        .catch(err => {
            this.setState({
                KPIdata:[],
                payloadData:[],  
                toaster:true,
                toastType:'error',
                title:'Network Error',
                text:'Error While Retrieving kpi Data',
            })
        })
    }

    loadData() {
        let inverterList = this.state.elementList;
        const { KPIdata, payloadData } =this.state
        if(!inverterList){
            return;
        }

        if(KPIdata.length){
            let sumPR = 0;
            let countPR=0;

            KPIdata.forEach((data) => {
                if (inverterList[data.device]) {
                    inverterList[data.device].id= data.device.split('_')[2];
                    inverterList[data.device].name = Utils.getDeviceName(data.device);
                    inverterList[data.device].PR = data.PR;
                    inverterList[data.device].kwhToday = data.kwhToday;
                    inverterList[data.device].kwhTillDate = data.kwhTillDate;
                    inverterList[data.device].yield = data.yield;
                    if (inverterList[data.device].PR || inverterList[data.device].PR===0) {
                        sumPR += inverterList[data.device].PR;
                        countPR++;
                    }
                }
            });
            let avgPR = sumPR / countPR;
            for (let  inverter in inverterList) {
                if(sumPR === 0){
                    inverterList[inverter].deviation = 0
                }else{
                    inverterList[inverter].deviation = ((inverterList[inverter].PR - avgPR) / avgPR) * 100;
                }
            }
        }
        let countInactive = 0;
        if(payloadData.length){
            payloadData.forEach((pData) => {
                if (inverterList[pData.device]) {
                    inverterList[pData.device].ACPower = pData.i15;
                    inverterList[pData.device].DCPower = pData.i21;
                    inverterList[pData.device].ts = pData.ts;
                    if (inverterList[pData.device].ACPower <=0) {
                        countInactive++;
                    }
                }
            });
        }
        let activeInvs = Object.keys(this.state.elementList).length - countInactive 
        this.setState({
            elementList: inverterList,
            activeInverters: activeInvs
        },() =>{
            this.loadPRGraph()
        });

    }

    loadACPowervsRadiation() {
        let acPowerData = this.state.acpowerData;
        let radiationData = this.state.radiationData;
        let acPower = [], radiation = [];

        acPowerData['MFM_ACPower'].forEach((acpower) => {
            acPower.push({ 'x': new Date((acpower.ts).substring(0, 16) + "Z").getTime(), 'y': Utils.round(acpower.map4, 2) });
        })
        radiationData['radiation'].forEach((data) => {
            radiation.push({ 'x': new Date((data.ts).substring(0, 16) + "Z").getTime(), 'y': Utils.round(data.w21, 2) });
        })

        let acPowerVsRadiationConfig = {
            chart: {
                zoomType: 'x',
            },
            title: {
                text: 'AC Power Vs Irradiance'
            },
            xAxis: {
                gridLineWidth: 1,
                type: 'datetime',
                title: {
                    text: 'Time'
                }

            },
            yAxis: [
                { // Primary yAxis
                    labels: {
                        format: '{value}  W/m²',
                        style: {
                            color: '#1DE9B6'
                        }
                    },
                    title: {
                        text: '',
                    },
                    opposite: true,
                    startOnTick: false
                },
                { // Secondary yAxis
                    labels:
                    {
                        format: '{value} kW',
                        style: {
                            color: '#bb6a88'
                        }
                    },
                    title: {
                        text: '',
                    },
                    startOnTick: false
                },
            ],
            exporting: {
                filename: 'AC Power Vs Irradiance - '+moment(this.state.startDate).format('DD-MMM-YYYY'),
                chartOptions: {
                    title: {
                        text: 'AC Power Vs Irradiance - '+Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']
                    }
                }
            },

            tooltip: {
                useHTML: true,
                shared: true,
                crosshairs: true,
            },
            plotOptions: {
                series: {
                    turboThreshold: 3000,
                }
            },
            series: [{
                name: 'Radiation ( W/m²)',
                type: 'spline',
                data: radiation, yAxis: 0,
                tooltip: {
                    valueSuffix: ' W/m²'
                },
            }, {
                name: 'AC Power (kW)',
                type: 'spline',
                data: acPower, yAxis: 1,
                tooltip: {
                    valueSuffix: ' kW'
                }
            }]
        }
        this.setState({
            acPowerVsRadiationChartConfig: acPowerVsRadiationConfig,
        })
    }

    getACPowerSeries(mfm, mfmData){
        let acPowerData = [];
        // let radiation = [];
        mfmData['MFM'].forEach((data) => {
            if(data.m32 || data.m32 == 0){
                acPowerData.push({ 'x': new Date((data.ts).substring(0, 16) + "Z").getTime(), 'y': Utils.round(data.m32, 2) });
            }
        })
        let series = {
            name: Utils.getDeviceName(mfm)+' AC Power (kW)',
            type: 'spline',
            data: acPowerData, yAxis: 1,
        }
        return series;
    }

    getIrradianceSeries(wms, wmsData){

        let irr = [];
        wmsData['WMS'].forEach((data) => {
            if(data.w21 || data.w21 == 0){
                irr.push({ 'x': new Date((data.ts).substring(0, 16) + "Z").getTime(), 'y': Utils.round(data.w21, 2) });
            }
        })
        let series = {
            name: 'Irradiance ( W/m²)',
            type: 'spline',
            data: irr, yAxis: 0,
        }
        return series;
    }


    loadACpowerIrradiance(customDate){

        const {defaultWMSSite,defaultWMS,OGMFMs} =this.state;
        let mfmCalls = [];
        let wmsCalls = [];
        OGMFMs.forEach(mfm=>{
            let url = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/' + Utils.getSite() + '/data/' + customDate + '/' + mfm+'_' + customDate + '.json?tags='+Object.keys(MFMMapping).toString();
            mfmCalls.push(axios.get(url,{withCredentials:true}));
        })
        let wmsSite = defaultWMSSite ||  Utils.getSite();
        defaultWMS.forEach(wms=>{
            let url = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/' + wmsSite + '/data/' + customDate + '/' + wms +'_' + customDate + '.json?tags=' + Object.keys(WMSMapping).toString();
            wmsCalls.push(axios.get(url,{withCredentials:true}));
        })
        let callbackCount = 0;
        let done = () =>{
            callbackCount++;
            if(callbackCount===2){
                this.processACvsRadiation();
                this.loadWMS()
            }
        }
        axios.all(mfmCalls)
            .then((res)=>{
                let mfmData = {};
                let sumOfACpower = 0
                res.forEach((mfm, index)=>{
                    mfmData[OGMFMs[index]]= mfm.data;
                    let acpower = 0
                    if(mfm.data.MFM[mfm.data.MFM.length - 1]){
                        acpower = mfm.data.MFM[mfm.data.MFM.length - 1].m32
                    }
                    sumOfACpower += acpower
                });
                this.setState({
                    mfmData:mfmData,
                    activePower:sumOfACpower
                },()=>{
                    done();
                })
            })
            .catch(err => {
                this.setState({
                    activePower:'NA',
                    mfmData:[],
                    toaster:true,
                    toastType:'error',
                    title:'Network Error',
                    text:'Error While Retrieving MFM Data',
                })
            })
        axios.all(wmsCalls)
            .then((res)=>{
                let wmsData = {};
                res.forEach((wms, index)=>{
                    wmsData[defaultWMS[index]]= wms.data;
                })
                this.setState({
                    wmsIrradianceData:wmsData
                },()=>{
                    done();
                })
            })
            .catch(err => {
                this.setState({
                    wmsIrradianceData:[],
                    toaster:true,
                    toastType:'error',
                    title:'Network Error',
                    text:'Error While Retrieving wmsIrradiance Data',
                })
            })   
    }

    processACvsRadiation(){

        const {mfmData,wmsIrradianceData} = this.state;
        let mfmSeries=[];
        let wmsSeries =[];
        Object.keys(mfmData).forEach((mfm)=>{
            let temp = this.getACPowerSeries(mfm,mfmData[mfm])
            mfmSeries.push(temp)
        });
        Object.keys(wmsIrradianceData).forEach((wms)=>{
            let temp = this.getIrradianceSeries(wms,wmsIrradianceData[wms])
            wmsSeries.push(temp)
        });
        let series = [...mfmSeries,...wmsSeries]
        let acPowerVsRadiationConfig = {
            chart: {
                zoomType: 'x',
            
            },
            title: {
                text: 'AC Power Vs Irradiance'
            },
            xAxis: {
                gridLineWidth: 1,
                type: 'datetime',
                title: {
                    text: 'Time'
                }
            },
            yAxis: [
                { // Primary yAxis
                    labels: {
                        format: '{value}  W/m²',
                        style: {
                            color: '#1DE9B6'
                        }
                    },
                    title: {
                        text: '',
                    },
                    opposite: true
                },
                { // Secondary yAxis
                    labels:
                    {
                        format: '{value} kW',
                        style: {
                            color: '#bb6a88'
                        }
                    },
                    title: {
                        text: '',
                    },
                },
            ],
            exporting: {
                filename: 'AC Power Vs Irradiance - '+moment(this.state.startDate).format('DD-MMM-YYYY'),
                chartOptions: {
                    title: {
                        text: 'AC Power Vs Irradiance - '+Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']
                    }
                }
            },
            tooltip: {
                useHTML: true,
                shared: true,
                crosshairs: true,
            },
            plotOptions: {
                series: {
                    turboThreshold: 3000,
                }
            },
            series: series
        }

        this.setState({
            acPowerVsRadiationChartConfig: acPowerVsRadiationConfig,
        })
    }

    loadPlantStatus(customDate){
        
        let plantKpiAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH +`/api/`+Utils.getSite()+`/kpi/Plant?dateFrom=`+customDate+`&dateTo=`+ customDate;

        let monthlyKPIAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/'+Utils.getSite()+'/kpi/Plant?summary=month&dateFrom='+customDate+'&dateTo='+ customDate;
        let monthlyEstimationKPIAPI = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/'+ Utils.getSite() + '/estimated-kpi?summary=month&dateFrom='+customDate+'&dateTo='+ customDate;
        
        axios.all([axios.get(plantKpiAPI),axios.get(monthlyKPIAPI),axios.get(monthlyEstimationKPIAPI)], { withCredentials: true })
            .then(axios.spread((plantKpi,monthlyKPI,monthlyEstimationKPI) => {
                this.setState({
                    plantKpiData:  plantKpi.data[0] || {},
                    monthlyKPIData: monthlyKPI.data[0] || {},
                    monthlyEstimationKPIData: monthlyEstimationKPI.data[0] || {}
                })
            }))
            .catch(err => {
                this.setState({
                    plantKpiData: {},
                    monthlyKPIData: {},
                    monthlyEstimationKPIData:{},
                    toaster:true,
                    toastType:'error',
                    title:'Network Error',
                    text:'Error While Retrieving monthlyKPI Data',                  
                })  
        })
    }

    loadPRGraph(customDate) {

        let dataStreams = this.state.elementList;
        let pr = [], inverterYield = [], energy = [], inverters = [];
        Object.keys(dataStreams).map((invId,index) => {
            pr.push(Utils.round(dataStreams[invId].PR, 2));
            inverterYield.push(Utils.round(dataStreams[invId].yield, 2));
            energy.push(Utils.round(dataStreams[invId].kwhToday, 2));
            inverters.push(dataStreams[invId].alias)
        })
        let inverterKpiConfig = {
            chart: {
                zoomType: 'x',
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Inverter Energy Vs Inverter PR Graph'
            },
            xAxis: [{
                categories: inverters,
                crosshair: true
            }],
            yAxis: [
                {
                    gridLineWidth: 0,
                    title: {
                        text: 'Inverter Energy (kWh)',
                        style: {
                            color: '#1DE9B6'
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: '#1DE9B6'
                        }
                    }
                }, {
                    gridLineWidth: 0,
                    title: {
                        text: 'Inverter PR (%)',
                        style: {
                            color: '#bb6a88'
                        }
                    },
                    labels: {
                        format: '{value} %',
                        style: {
                            color: '#bb6a88'
                        }
                    },
                    opposite: true,
                    min: 0
                }, {
                    gridLineWidth: 0,
                    title: {
                        text: 'Inverter Yield (kWh/kWp)',
                        style: {
                            color: '#1DC4E9'
                        }
                    },
                    labels: {
                        format: '{value} ',
                        style: {
                            color: '#1DC4E9'
                        }
                    }

                }],
            exporting: {
                filename: 'Inverter Energy Vs Inverter PR - '+moment(this.state.startDate).format('DD-MMM-YYYY'),
                chartOptions: {
                    title: {
                        text: 'Inverter Energy Vs Inverter PR - '+Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']
                    }
                }
            },
            tooltip: {
                useHTML: true,
                crosshairs: true,
                shared: true,
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom'
            },
            series: [{
                name: 'Inverter Energy (kWh)',
                type: 'column',
                data: energy, lineWidth: 1,
            }, {
                name: 'Inverter PR (%)',
                type: 'spline',
                data: pr, yAxis: 1, lineWidth: 2,
            }, {
                name: 'Inverter Yield (kWh/kWp)',
                type: 'spline',
                data: inverterYield, yAxis: 2, lineWidth: 2,
            }],
        }
        this.setState({
            inverterKpiChartConfig: inverterKpiConfig,
        })
    }

    loadWMS() {
        let formattedData = []
        let seriesData = [];
        const {wmsIrradianceData} = this.state

        wmsIrradianceData[Object.keys(wmsIrradianceData)[0]]["WMS"].forEach((dataStream) => {
            let obj = {};
            Object.keys(dataStream).forEach(function (key) {
                if (key === 'w2' || key === 'w5' || key === 'w23') {
                    return;
                }
                else if (key === 'ts') {
                    obj[key] = dataStream[key]
                } else if (dataStream[key] || dataStream[key] == 0 || dataStream[key] == false) {
                    if (dataStream[key] < 2000) {
                        obj[WMSMapping[key]] = dataStream[key] ? Math.round(dataStream[key] * 100) / 100 : dataStream[key];
                    }
                }
            });
            formattedData.push(obj)
        })
        let keys = Object.values(WMSMapping);
        keys.forEach(function (key, index) {
            if (key === 'ts') return;
            let data = []

            formattedData.forEach(function (item) {
                if (item[key] || item[key] === 0 || item[key] == false) {
                    let unixTime = new Date(item['ts'].substring(0, 16) + "Z").getTime()
                    data.push([unixTime, item[key]])
                }
            })

            if (!data.length) return;
            let obj;
            if (key === "GHI") {
                obj = {
                    "name": key + " (W/m²)",
                    "data": data,
                }
            } else if (key === "POA_1") {
                obj = {
                    "name": "POA 1 (W/m²)",
                    "data": data,
                }
            }
            else if (key === "POA_2") {
                obj = {
                    "name": "POA 2 (W/m²)",
                    "data": data,
                }
            }
            else if (key === "POA_3") {
                obj = {
                    "name": "POA 3 (W/m²)",
                    "data": data,
                }
            }
            else if (key === "Mod_Temp_1") {
                obj = {
                    "name": "Mod Temp 1 (°C)",
                    "data": data,
                    "yAxis": 1,
                }
            }
            else if (key === "Mod_Temp_2") {
                obj = {
                    "name": "Mod Temp 2 (°C)",
                    "data": data,
                    "yAxis": 1,
                }
            }
            else if (key === "Amb_Temp") {
                obj = {
                    "name": "Amb Temp (°C)",
                    "data": data,
                    "yAxis": 1,
                }
            }
            else if (key === "Wind_Speed") {
                obj = {
                    "name": "Wind Speed (m/s)",
                    "data": data,
                    "yAxis": 3,
                    "visible": false,
                }
            } else if (key === "Wind_Direction") {

                obj = {
                    "name": "Wind Direction (°)",
                    "data": data,
                    "yAxis": 2,
                    "visible": false,
                }
            }

            if (obj) {
                seriesData.push(obj)
            }

        })

        let wmsConfig = {
            credits: {
                enabled: false
            },
            chart: {
                renderTo: 'container',
                zoomType: 'x'
            },
            title: { text: 'Weather Monitoring System' },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f} W/m²',
                    style: {
                        color: 'black'
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: "black"
                    }
                },
                min: 0
            }, { // Primary yAxis
                labels: {
                    format: '{value:,.0f} °C',
                    style: {
                        color: "black"
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: "black"
                    }
                },
                min: 0,
                opposite: true
            }, { // Primary yAxis
                labels: {
                    format: '{value:,.0f} °',
                    style: {
                        color: "#F44236"
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: "#F44236"
                    }
                },
                min: 0
            }, { // Primary yAxis
                labels: {
                    format: '{value:,.0f} m/s',
                    style: {
                        color: " #899fd4"
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: " #899fd4"
                    }
                },
                min: 0,
                opposite: true
            }, { // Primary yAxis
                labels: {
                    format: '{value:,.0f} mm',
                    style: {
                        color: "#F15C80"
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: "#F15C80"
                    }
                },
                min: 0
            }, { // Primary yAxis
                labels: {
                    format: '{value:,.0f}%',
                    style: {
                        color: "#899FD4"
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: "#899FD4"
                    }
                },
                min: 0,
                opposite: true
            }, { // Primary yAxis
                labels: {
                    format: '{value:,.0f}V',
                    style: {
                        color: Highcharts.getOptions().colors[6]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[6]
                    }
                },
                min: 0,
                opposite: true
            }, { // Primary yAxis
                labels: {
                    format: '{value:,.0f}kW',
                    style: {
                        color: Highcharts.getOptions().colors[6]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[6]
                    }
                },
                min: 0,
                opposite: true
            }

            ],
            exporting: {
                filename: 'Weather Monitoring System - '+moment(this.state.startDate).format('DD-MMM-YYYY'),
                chartOptions: {
                    title: {
                        text: 'Weather Monitoring System - '+Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']
                    }
                }
            },
            tooltip: {
                useHTML: true,
                shared: true,
                crosshairs: true,
            },
            xAxis: {
                gridLineWidth: 1,
                type: 'datetime',
                title: {
                    text: 'Time'
                }

            },
            series: seriesData
        }

        this.setState({
            wmsChartConfig: wmsConfig
        })
    }

    render() {
        const { toaster,toastType,title,text,activeInverters, elementList,plantName, inverterKpiChartConfig, wmsChartConfig, acPowerVsRadiationChartConfig, wmsIrradianceData, mfmData, plantKpiData, monthlyKPIData, monthlyEstimationKPIData, activePower } = this.state
        return (
            <Aux>
                <Row>
                    <Col xl={12}>
                        <div className="page-header">
                            <div className="page-block">
                                <Row className="align-items-center">
                                    <Col md={12} lg={12}>
                                        <div className="page-header-title m-t-30">
                                            <h5 className="m-b-10 float-left">Dashboard</h5>
                                            <div className='pull-right'>
                                                <div className="date-picker">
                                                    <DateRangePicker
                                                        singleDatePicker={true}
                                                        startDate={this.state.startDate}
                                                        minDate='01/01/2012'
                                                        maxDate={this.state.maxDate}
                                                        showWeekNumbers={false}
                                                        opens={'left'}
                                                        showDropdowns={true}
                                                        onApply={this.handleCustomDate}
                                                        timezone= {moment.tz.setDefault("Asia/Kolkata")} //{( Utils.getSiteMetaData(Utils.getSite()) && Utils.getSiteMetaData(Utils.getSite())["timeZone"] )? moment.tz.setDefault(Utils.getSiteMetaData(Utils.getSite())["timeZone"]):moment.tz.setDefault("Asia/Kolkata")}
                                                        >

                                                    <Button className='m-0 btn-sm mt-0'>
                                                        <i className={'feather icon-calendar'} />
                                                        {moment(this.state.startDate).format('DD-MMM-YYYY')}<b className="fa fa-caret-down ml-2"></b>
                                                    </Button>
                                                    </DateRangePicker>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Breadcrumb className='mr-auto'>
                                        <Breadcrumb.Item href={'#'}><i className="feather icon-home" /></Breadcrumb.Item>
                                        <Breadcrumb.Item active>Dashboard</Breadcrumb.Item>
                                    </Breadcrumb>
                                </Row>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} xl={4}>
                        <Card className='Active-visitor'>
                            <Card.Body className='border-bottom bg-c-green'>
                                <div className="row d-flex align-items-center">
                                    <div className="col-auto">
                                        <i className="fa fa-bolt f-40 text-light"/>
                                    </div>
                                    <div className="col">
                                        {/* {plantKpiData ? <h2 className={plantKpiData &&(plantKpiData.exportToday/1000 < 0) ? 'f-w-300 text-danger':'f-w-300 text-light'}>
                                            {(plantKpiData && (plantKpiData.exportToday || plantKpiData.exportToday === 0)) ? Utils.round(plantKpiData.exportToday/1000,2) : 'NA'}
                                            <span className="f-14 text-light">{(plantKpiData && (plantKpiData.exportToday || plantKpiData.exportToday === 0)) ? ' MWh' : 'NA'} </span>
                                        </h2>:<SmallSpinner/>} */}
                                        <h2 className="f-w-300 text-light">
                                            1,419,628
                                            <span className="f-14 text-light">Users</span>
                                        </h2>
                                        <span className="d-block text-light">
                                            {/* {(moment(new Date()).format('DD-MMM-YYYY') === moment(this.state.startDate).format('DD-MMM-YYYY')) ? "Today's Export" : moment(this.state.startDate).format('DD-MMM-YYYY') + ' Export'} */}
                                           Ooreedo
                                        </span>
                                    </div>
                                </div>
                            </Card.Body>
                            <Card.Body className="py-3">
                                <div className="row d-flex card-active align-items-center pb-3 border-bottom">
                                    <div className="col-md-6 col-6 text-center">
                                        {/* {plantKpiData?<h4 className={(plantKpiData && (plantKpiData.importToday/1000 < 0)) ? "text-danger" : 'text-muted'}>{(plantKpiData && (plantKpiData.importToday || plantKpiData.importToday === 0)) ? Utils.round(plantKpiData.importToday/1000, 2)+' MWh' : 'NA'} </h4>:<SmallSpinner/>}
                                        <span className="text-muted">{(moment(new Date()).format('DD-MMM-YYYY') == moment(this.state.startDate).format('DD-MMM-YYYY')) ?  "Today's Import" : moment(this.state.startDate).format('DD-MMM-YYYY')+' Import'}</span> */}
                                        <h4 className="text-muted"> 1,000,628 Users</h4>
                                        <span className="text-muted">5G Network</span>
                                    </div>
                                    <div className="col-md-6 col-6 text-center">
                                        {/* {plantKpiData?<h4 className='text-muted'>{(plantKpiData && (plantKpiData.maxAC || plantKpiData.maxAC === 0)) ? Utils.round(plantKpiData.maxAC/1000, 2)+' MW' : 'NA' } </h4>:<SmallSpinner/>}
                                        <span className="text-muted">Peak Power</span> */}
                                        <h4 className="text-muted">419,000 Users</h4>
                                        <span className="text-muted">Other Network</span>
                                    </div>
                                </div>
                                <Row className="pt-3 pb-2">
                                    <Col className="pt-2" sm={12}>
                                        <span>Network Availability</span>
                                        <span className="float-right text-muted">
                                            {/* {activePower !== null ? ((activePower!='NA' && (activePower  || activePower===0))?Utils.round(activePower/1000,2)+' MW' : 'NA') :<SmallSpinner/>}  */}
                                            Compliant
                                        </span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} xl={4}>
                        <Card className='Active-visitor'>
                            <Card.Body className='border-bottom bg-c-yellow'>
                                <div className="row d-flex align-items-center">
                                    <div className="col-auto">
                                        <i className="feather icon-sun f-40 text-dark"/>
                                    </div>
                                    <div className="col">
                                        {/* {plantKpiData ?<h2 className={(plantKpiData && (plantKpiData.insolationPOA >= 0 && plantKpiData.insolationPOA < 10 || !plantKpiData.insolationPOA))? "f-w-300 text-dark":"f-w-300 text-danger"}>
                                        {(plantKpiData && (plantKpiData.insolationPOA || plantKpiData.insolationPOA === 0)) ? Utils.round(plantKpiData.insolationPOA, 2) : 'NA'}
                                            <span className="f-14 text-dark">{(plantKpiData && (plantKpiData.insolationPOA || plantKpiData.insolationPOA === 0)) ? 'kWh/m²' : 'NA'} </span>
                                        </h2>:<SmallSpinner/>}
                                        <span className="d-block text-dark">Insolation</span> */}
                                        <h2 className="f-w-300 text-dark">1,162,797<span className="f-14 text-dark">Users</span></h2>
                                        <span className="d-block text-dark">Vodafone</span>
                                    </div>
                                </div>
                            </Card.Body>
                            <Card.Body className="py-3">
                                <div className="row d-flex card-active align-items-center pb-3 border-bottom">
                                    <div className="col-md-6 col-6 text-center">
                                       {/* {plantKpiData? <h4 className='text-muted'>{(plantKpiData && (plantKpiData.yield || plantKpiData.yield===0))?Utils.round(plantKpiData.yield,2)+' kWh/kWp' : 'NA'} </h4>:<SmallSpinner/>} */}
                                       <h4 className="text-muted">1,000,727 Users</h4>
                                        <span className="text-muted">5G Network</span>
                                    </div>
                                    <div className="col-md-6 col-6 text-center">
                                        {/* {plantKpiData?<h4 className='text-muted'>{ (plantKpiData && (plantKpiData.ACCUF || plantKpiData.ACCUF===0))?Utils.round(plantKpiData.ACCUF,2)+'  %' : 'NA' }</h4>:<SmallSpinner/>} */}
                                        <h4 className="text-muted">162,000 Users</h4>
                                        <span className="text-muted">Other Network</span>
                                    </div>
                                </div>
                                <Row className="pt-3 pb-2">
                                    <Col className="pt-2" sm={12}>
                                        <span>Network Availability</span>
                                        <span className="float-right text-muted">
                                            {/* { activeInverters !== null?(activeInverters  || activeInverters === 0 ?(<span className="">{activeInverters + (elementList? ('/'+Object.keys(elementList).length):"")}</span>):(<SmallSpinner/>)):<SmallSpinner/>} */}
                                            Compliant
                                        </span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} xl={4}>
                        <Card className='Active-visitor'>
                            <Card.Body className='border-bottom bg-c-blue'>
                                <div className="row d-flex align-items-center">
                                    <div className="col-auto">
                                        <i className="fa fa-line-chart f-40 text-light"/>
                                    </div>
                                    <div className="col">
                                        {/* {plantKpiData ?<h2 className={plantKpiData && (plantKpiData.PR > 0 && plantKpiData.PR < 100 || !plantKpiData.PR ) ? "f-w-300 text-light" : "f-w-300 text-danger"}>
                                        { (plantKpiData && (plantKpiData.PR || plantKpiData.PR === 0)) ? Utils.round(plantKpiData.PR, 2) : 'NA' }
                                            <span className="f-14 text-light">{ (plantKpiData && (plantKpiData.PR || plantKpiData.PR === 0)) ? '%': '' } </span>
                                        </h2>:<SmallSpinner/>}
                                        <span className="d-block text-light">Performance Ratio</span> */}
                                        <h2 className="f-w-300 text-light"> 2,582,425<span className="f-14 text-light">Users</span></h2>
                                        <span className="d-block text-light">Total Users</span>
                                    </div>
                                </div>
                            </Card.Body>
                            <Card.Body className="py-3">
                                <div className="row d-flex card-active align-items-center pb-3 border-bottom">
                                    <div className="col-md-6 col-6 text-center">
                                        {/* {plantKpiData ? <h4 className='text-muted'>{ (plantKpiData && (plantKpiData.pa || plantKpiData.pa===0))?Utils.round(plantKpiData.pa,2)+'  %' : 'NA' }</h4>:<SmallSpinner/>} */}
                                        <h4 className="text-muted">2,000,425 Users</h4>
                                        <span className="text-muted">5G Network</span>
                                    </div>
                                    <div className="col-md-6 col-6 text-center">
                                        {/* {plantKpiData ?<h4 className='text-muted'>{ (plantKpiData && (plantKpiData.ga || plantKpiData.ga===0))?Utils.round(plantKpiData.ga,2)+ ' %' : 'NA' }</h4>:<SmallSpinner/>} */}
                                        <h4 className="text-muted">582,000 Users</h4>
                                        <span className="text-muted">Other Network</span>
                                    </div>
                                </div>
                                <Row className="pt-3 pb-2">
                                    <Col className="pt-2" sm={12}>
                                        <span>Network Availability</span>
                                        <span className="float-right text-muted">
                                            {/* { plantKpiData?((plantKpiData && (plantKpiData.wPR || plantKpiData.wPR===0))?Utils.round(plantKpiData.wPR,2)+' %': 'NA'):<SmallSpinner/> } */}
                                            Compliant
                                        </span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                    </Row>
                    <Row>
                        <Col md={12} xl={12}>
                                <PanelwithName name="Zone Heat map, Network Status: Dark Green 5G | Light Green 4G | Pale Yellow 2G | Orange E | Black No Sufficient Data | ! No Network" 
                                // legend={<InverterLegend/>}
                                >
                                {
                                    !elementList ? (
                                        <div className="text-center">
                                            <Spinner />
                                        </div>
                                    ) : (
                                            <InverterGridList Inverters={elementList} elementAction={this.inverterClick} selectedDate={this.state.startDate} />
                                        )
                                }
                                </PanelwithName>
                        </Col>
                    </Row>
                    <Row>
                    <Col md={4} xl={4}>
                        <Card className='theme-bg'>
                            <Card.Body className='p-3'>
                                <h4 className="text-white text-center">Ooreede Monthly 5G Status</h4>
                                <div className="row p-t-10">
                                    <div className="col text-center">
                                <h4 className="text-white f-w-300">
                                    {/* { (monthlyEstimationKPIData && (monthlyEstimationKPIData.PR || monthlyEstimationKPIData.PR===0))?Utils.round(monthlyEstimationKPIData.PR,2) : 'NA'}<span className="f-12">{(monthlyEstimationKPIData && (monthlyEstimationKPIData.PR || monthlyEstimationKPIData.PR===0))?' %':''}</span> */}
                                    <span className="f-12">75%</span>
                                    </h4>
                                        <p className="text-white d-block">SLA Target</p>
                                    </div>

                                    <div className="col text-center">
                                        <h4 className="text-white  f-w-300">
                                            {/* { (monthlyKPIData && (monthlyKPIData.PR || monthlyKPIData.PR===0))?Utils.round(monthlyKPIData.PR,2) : 'NA'}<span className="f-12">{ (monthlyKPIData && (monthlyKPIData.PR || monthlyKPIData.PR===0))?' %':''}</span> */}
                                            <span className="f-12">77.98%</span>
                                        </h4>
                                        <p className="text-white d-block">Actual</p>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} xl={4}>
                        <Card className='theme-bg2'>
                            <Card.Body className='p-3'>
                                <h4 className="text-white text-center">Vodafone Monthly 5G Status</h4>
                                <div className="row p-t-10">
                                    <div className="col text-center">
                                        <h4 className="text-white f-w-300">
                                            {/* { 
                                                (monthlyEstimationKPIData && (monthlyEstimationKPIData.insolationPOA || monthlyEstimationKPIData.insolationPOA===0))?
                                                (moment(this.state.startDate).format('DD-MM-YYYY') == moment(new Date()).format('DD-MM-YYYY'))?
                                                    Utils.round(monthlyEstimationKPIData.insolationPOA*new Date().getDate()/moment().daysInMonth(),2)
                                                    :Utils.round(monthlyEstimationKPIData.insolationPOA,2) 
                                                : 'NA'
                                            }  */}
                                                {/* <span className="f-12">{(monthlyEstimationKPIData && (monthlyEstimationKPIData.insolationPOA || monthlyEstimationKPIData.insolationPOA===0)) ? ' kWh/m²': ''} </span> */}
                                                <span className="f-12">76%</span>
                                        </h4>
                                        <p className="text-white d-block">SLA Target</p>
                                    </div>

                                    <div className="col text-center">
                                        <h4 className="text-white  f-w-300">
                                            {/* { (monthlyKPIData && (monthlyKPIData.insolationPOA || monthlyKPIData.insolationPOA===0))?Utils.round(monthlyKPIData.insolationPOA,2) : 'NA'}<span className="f-12">{(monthlyKPIData && (monthlyKPIData.insolationPOA || monthlyKPIData.insolationPOA===0))?' kWh/m²' : ''}</span> */}
                                            <span className="f-12">87.66%</span>
                                            </h4>
                                        <p className="text-white d-block">Actual</p>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} xl={4}>
                        <Card className='bg-c-blue'>
                            <Card.Body className='p-3'>
                                <h4 className="text-white text-center">All Monthly 5G Status</h4>
                                <div className="row p-t-10">
                                    <div className="col text-center">
                                        <h4 className="text-white f-w-300">
                                            {/* { (monthlyEstimationKPIData && (monthlyEstimationKPIData.ACCUF || monthlyEstimationKPIData.ACCUF===0))?Utils.round(monthlyEstimationKPIData.ACCUF,2) : 'NA'}<span className="f-12"> { (monthlyEstimationKPIData && (monthlyEstimationKPIData.ACCUF || monthlyEstimationKPIData.ACCUF===0))?' %' : ''}</span> */}
                                            <span className="f-12">75.5%</span>
                                            </h4>
                                        <p className="text-white d-block">SLA Target</p>
                                    </div>

                                    <div className="col text-center">
                                        <h4 className="text-white  f-w-300">
                                            {/* { (monthlyKPIData && (monthlyKPIData.ACCUF || monthlyKPIData.ACCUF===0))?Utils.round(monthlyKPIData.ACCUF,2) : 'NA'}<span className="f-12"> { (monthlyKPIData && (monthlyKPIData.ACCUF || monthlyKPIData.ACCUF===0))?' %' : ''}</span> */}
                                            <span className="f-12">82%</span>
                                            </h4>
                                        <p className="text-white d-block">Actual</p>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    </Row>
                    {/* <Row>
                    <Col xl={6}>
                        <Card>
                            <Card.Header className='filter-bar'>
                                <Card.Title as='h5'>AC Power Vs Irradiance 
                                <a onClick={() => this.toMFMModule(this.state.startDate)} className='label label-info cursor ml-1 rounded' title='MFM Dashboard'><i className="fa fa-line-chart text-white"></i>
                                </a>
                                </Card.Title>
                            </Card.Header>
                            <Card.Body>
                            {
                                !mfmData && !wmsIrradianceData ? (
                                    <div className="text-center">
                                        <Spinner />
                                    </div>
                                ) : (
                                        <Chart chartConfig={acPowerVsRadiationChartConfig} style={{ style: { height: "400px" } }}></Chart>
                                    )
                            }
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xl={6}>
                        <Card>
                            <Card.Header>
                                <Card.Title as='h5'>Inverter Energy Vs Inverter PR Graph</Card.Title>
                            </Card.Header>
                            <Card.Body>
                            {
                                !elementList ? (
                                    <div className="text-center">
                                        <Spinner />
                                    </div>
                                ) : (
                                        <Chart chartConfig={inverterKpiChartConfig} style={{ style: { height: "400px" } }}></Chart>
                                    )
                            }
                            </Card.Body>
                        </Card>
                    </Col>
                    </Row>
                    <Row>
                    <Col xl={12}>
                        <Card>
                            <Card.Header  className='filter-bar'>
                                <Card.Title as='h5'>Weather Monitoring System
                                <a onClick={() => this.toWMSModule(this.state.startDate)} className='label label-info cursor ml-1 rounded' title='WMS Dashboard'><i className="fa fa-line-chart text-white"></i>
                                </a>
                                </Card.Title>
                            </Card.Header>
                            <Card.Body>
                            {
                                    !wmsIrradianceData ? (
                                        <div className="text-center">
                                            <Spinner />
                                        </div>
                                    ) : (
                                            <Chart chartConfig={wmsChartConfig} style={{ style: { height: "400px" } }}></Chart>
                                        )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                    </Row> */}
            </Aux>
        );
    }
}

export default Dashboard;