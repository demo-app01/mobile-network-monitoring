import React, { Component } from 'react'
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import moment from 'moment';
import Utils from './../../Utils';
import 'moment-timezone';
import addNoDataModule from 'highcharts/modules/no-data-to-display';
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/export-data')(Highcharts)

addNoDataModule(Highcharts);

window.moment = moment;
var url=window.location.pathname.split('/');


export class Chart extends Component {

shouldComponentUpdate = (nextProps) => {
  if (nextProps.chartConfig !== this.props.chartConfig) {
    return true;
  }
  return false;
}

render() {

  var customColors = ['#1de9b6','#f44236', '#f4c22b','#474787','#1dc4e9',  '#7d5fff','#f15c80', '#2b908f','#90ed7d',  '#bb6a88', '#91e8e1',"#7cb5ec","#f7a35c", "#8085e9", "#e4d354", "#f45b5b"]  
  //HighChart Component 

  let chartTitle=this.props.chartTitle
  let date=this.props.date
  let displayName=this.props.plantDisplayName
  let dateFormat=this.props.tooltipFormat ? this.props.tooltipFormat:'%A, %b %d, %Y %H:%M'
  let title;
  // global options
  Highcharts.setOptions({
      time:{
          timezone: "Asia/Kolkata"
      },
      credits: {
          enabled: false
      }, 
      tooltip: {
        xDateFormat: dateFormat,
        useHTML: true,
      },        
      chart: { 
          resetZoomButton: {
            position: {
              x: 0,
              y: -50
          }
        },
        
          events: {
          beforePrint: function(e) {
          title=e.target.userOptions.title.text
  
            this.setTitle({
              text: title+' - '+url[1]
            })
          },
          afterPrint: function() {
  
            this.setTitle({
              text: title
            })
          }
        }
      },
      
      exporting: {
          filename: chartTitle+" - "+date,
          chartOptions:{
          title: {
              text:chartTitle+", "+displayName
          }
      },
          sourceWidth: window.innerWidth,
          sourceHeight: 500,
          scale: 1,
          buttons: {
            contextButton: {
              menuItems: [
                'viewFullscreen',
                'printChart',
                'downloadPNG',
                'downloadJPEG',
                'downloadPDF',
                'downloadSVG',
                'downloadCSV',
                'downloadXLS',
              ]
            }
          }
      },
  })

  //set chart colors
  this.props.chartConfig.colors = Highcharts.map(customColors, (color) => {
    return this.props.darkenColors ? {
      linearGradient: { },
        stops: [
            [0, this.props.isGradient ? color: Highcharts.Color(color).brighten(-0.2).get('rgb')],
            [1, Highcharts.Color(color).brighten(-0.2).get('rgb')] // darken
        ]
    } :
    color
  })

  
  if(this.props.chartConfig && this.props.chartConfig.tooltip && !this.props.chartConfig.tooltip.positioner){
      
      this.props.chartConfig.tooltip.positioner= function(labelWidth, labelHeight, point) {
        let x = point.plotX;
        let y = point.plotY;

        if(x>this.chart.plotWidth/2){
          x=x-labelWidth+10;
        }
        else
          x=x-10;
        if(y>this.chart.plotHeight/2)
          y=y-labelHeight+10;
        else
          y=y-10;
        
        x=x+this.chart.plotLeft;
        y=y+this.chart.plotTop;

          // if (point.plotX - labelWidth / 2 > 0) {
          //     x = point.plotX - labelWidth / 2;
          // } else {
          //     x = 0
          // }
          return {
              x: x,
              y: y
          }
      }
  }
    return (
      <HighchartsReact highcharts={Highcharts}  options={this.props.chartConfig} 
            containerProps={this.props.style}
            />
    )
  }
}

export default Chart




