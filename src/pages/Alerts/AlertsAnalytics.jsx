import React, { Component } from 'react';
import Utils from "../../Utils";
import { Link } from 'react-router-dom';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import "bootstrap-daterangepicker/daterangepicker.css";
import moment from 'moment';
import alertTypes from './alertTypes-config';
import Select from 'react-select';
import Highcharts from 'highcharts';
import axios from 'axios';
import Chart from './Chart';
// import {Event} from '../../Tracking/index';

import { Row, Col, Card, Button, Breadcrumb} from 'react-bootstrap';
import Aux from "../../hoc/_Aux";
import Spinner from "../Spinner";
// import Toaster from "../../../actions/Toaster";

require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/drilldown')(Highcharts);

let list = require('./dropDownDeviceList.json');

let firstChart = {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: ''
    },
    xAxis: [{
        categories: ['Doha', 'Al Rayyan', 'Umm Salal Muhammad', 'Al Wakrah', 'Al Khor', 'Al-Sahaniya',
            'Dukhan', 'Zone 9', 'Zone 10', 'Zone 11', 'Zone 12', 'Zone 13'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Count',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: '.',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    // legend: {
    //     layout: 'vertical',
    //     align: 'left',
    //     x: 120,
    //     verticalAlign: 'top',
    //     y: 100,
    //     floating: true,
    //     backgroundColor:
    //         Highcharts.defaultOptions.legend.backgroundColor || // theme
    //         'rgba(255,255,255,0.25)'
    // },
    series: [{
        name: '% of people affected',
        type: 'column',
        yAxis: 1,
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
        tooltip: {
            valueSuffix: ''
        }

    }, {
        name: 'Number of people affected',
        type: 'spline',
        data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9],
        tooltip: {
            valueSuffix: ''
        }
    }]
}

let secondChart = {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: ''
    },
    xAxis: [{
        // categories: ['Doha', 'Al Rayyan', 'Umm Salal Muhammad', 'Al Wakrah', 'Al Khor', 'Al-Sahaniya',
        //     'Dukhan', 'Zone 9', 'Zone 10', 'Zone 11', 'Zone 12', 'Zone 13'],
        categories: ['1-Sep-21', '2-Sep-21', '3-Sep-21', '4-Sep-21', '5-Sep-21', '6-Sep-21',
            '7-Sep-21', '8-Sep-21', '9-Sep-21', '10-Sep-21', '11-Sep-21', '12-Sep-21'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Count',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Time(Minutes)',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    // legend: {
    //     layout: 'vertical',
    //     align: 'left',
    //     x: 120,
    //     verticalAlign: 'top',
    //     y: 100,
    //     floating: true,
    //     backgroundColor:
    //         Highcharts.defaultOptions.legend.backgroundColor || // theme
    //         'rgba(255,255,255,0.25)'
    // },
    series: [{
        name: 'Alerts',
        type: 'column',
        yAxis: 1,
        data: [30, 10, 50, 5, 10, 3, 8, 90, 110, 150, 180, 120],
        tooltip: {
            valueSuffix: ''
        }

    }, {
        name: 'Alerts Avg Duration (minutes)',
        type: 'spline',
        data: [20, 10, 10, 10, 12, 15, 20, 18, 23, 18, 13, 9],
        tooltip: {
            valueSuffix: ''
        }
    }]
}

class AlertsAnalytics extends Component {

    constructor(props) {
        super(props);
        var arrAlertsTypes = [];
        var alertOptions = [];
        var deviceOptions = [];

        Object.keys(alertTypes).forEach(function (element, key) {
            if(element!='openAlertIcon' && element!='closedAlertIcon' && element!='yPostion'){
                arrAlertsTypes.push(element);
            } 
        });
        // alertOptions.push({ 'value': null, 'label': 'All Alerts' })
        // deviceOptions.push({ 'value': null, 'label': 'All Devices' })
        arrAlertsTypes.map((element, index) =>
            alertOptions.push({ 'value': element, 'label': alertTypes[element] })     
        )

        let inverterDevices= list;

        // inverterDevices.forEach((element, index) =>
        //     deviceOptions.push({ 'value': element, 'label': Utils.getDeviceName(element) })
        // )
        this.state = {
            alertOptions:alertOptions,
            deviceOptions:deviceOptions,
            displaytext:'',
            perday : '',
            perdaytype : '',
            text:'',
            showResults: false,
            startDate: moment().startOf('month').toDate(),
            endDate: moment(new Date()).toDate(),
            MaxDate: moment(new Date()).toDate(),
            inverterDevices: [],
            SelectiveDeviceName: { 'value': null, 'label': 'All Zones' },
            SelectivealertType: { 'value': null, 'label': 'Network Switch' },
            alertTypeChartConfig: {},
            data: [],
            alertSummary: {},
            spinner: false,
            barOptionsDeviceAlerts: {
                credits: {
                    enabled: false
                },
                yAxis:{
                    title:{
                        text:'Count'
                    }
                },
                title: {
                    text: 'Alerts Count'
                },
                lang: {
                    noData: "Please Choose above options to load data"
                },
                series: [{
                    showInLegend: true,
                    name:'Alerts',
                    data:[]
                }],
            },
            deviceAndAlertChartOptions: {
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Alerts Analytics'
                },
                yAxis:{
                    title:{
                        text:'Count'
                    }
                },
                lang: {
                    noData: "Please Choose above options to load data"
                },
                series: [{
                    showInLegend: true,
                    name:'Alerts',
                    data:[]
                }],
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            
        }
        this.handleCustomDate = this.handleCustomDate.bind(this)
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.toMonitorInverter = this.toMonitorInverter.bind(this);
    }


    toMonitorInverter(device,startDate,endDate){
     let customDate
        let startDateTo=Utils.yyyymmdd(startDate);
        let endDateTo=Utils.yyyymmdd(endDate);
        if(startDateTo==endDateTo){
            customDate=startDateTo
        }else{  
            
            customDate=Utils.yyyymmdd(moment(new Date()).toDate());
        }

        let inverterId = device.value
        let path = '/' + Utils.getSite() + '/inverter/dashboard'
        this.props.history.push({pathname:path,hash:'#'+inverterId+'#'+customDate})
        

    
    }
    
    componentDidMount() {

        // const{data} = this.props.history.location
        // if(!data){
        //     this.setState({
        //         SelectiveDeviceName: { 'value': Utils.getInverterdevices()[0], 'label': Utils.getDeviceName(Utils.getInverterdevices()[0]) },
        //         SelectivealertType: { 'value': null, 'label': 'All Alerts' },
        //         plantName : Utils.getSiteMetaData(Utils.getSite())["plantDisplayName"],
        //     })
        // }
        // else{
        //     if(data.device && data.alert){           
        //         this.setState({
        //             startDate: moment(data.startDate).toDate(),
        //             endDate:moment(data.startDate).toDate(),
        //             SelectiveDeviceName:{ 'value': data.device, 'label': Utils.getDeviceName(data.device) },
        //             SelectivealertType:{ 'value':data.alert, 'label': alertTypes[data.alert]},            
        //         },()=>{
        //             this.onSubmit()
        //         })
        //     }else if(data.device){

        //         let selectedDeviceTypeIndex = this.state.deviceOptions.findIndex((deviceType)=>{
        //             return deviceType.value === data.device;
        //         })
         
        //         this.setState({
        //             SelectiveDeviceName: this.state.deviceOptions[selectedDeviceTypeIndex] ,
        //             SelectivealertType: { 'value': null, 'label': 'All Alerts' },
        //             startDate:moment(data.startDate).toDate(),
        //             endDate:moment(data.startDate).toDate(),

        //         },()=>{
        //             this.onSubmit()
        //         })
        //     }else{

        //         let selectedAlertTypeIndex = this.state.alertOptions.findIndex((alertType)=>{
        //             return alertType.value === data.alert;
        //         })
        //         this.setState({
        //             SelectiveDeviceName: { 'value': null, 'label': 'All Devices' },
        //             SelectivealertType:this.state.alertOptions[selectedAlertTypeIndex]
                   
        //         },()=>{
        //             this.onSubmit()
        //         })
        //     }
            
        // }

        // this.setState({
        //     plantName : Utils.getSiteMetaData(Utils.getSite())["plantDisplayName"]
        // })
        
    }

    handleCustomDate(event, picker) {
        this.setState({
            startDate:moment(picker.startDate).toDate(),
            endDate: moment(picker.endDate).toDate()
        }, () => {

        })
    }

    
    handleDevice = SelectiveDeviceName => {
        this.setState({SelectiveDeviceName});    
    }


    handleAlertType = SelectivealertType => {     
        this.setState({ SelectivealertType });          
    }

    

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        });
    }

    onSubmit = () => {    
        if(this.state.SelectiveDeviceName.value != undefined && this.state.SelectivealertType.value != undefined ){ 
              
            var typeSelection = this.state.SelectivealertType.label.concat(" Alerts on ");  
            var concatBoth = typeSelection.concat(this.state.SelectiveDeviceName.label);    
            this.setState({
                spinner: true,
                displaytext : concatBoth,
                perday : concatBoth,
                showResults: true,
            },()=>{
                this.loadAlerts(null, () => {          
                    this.state.SelectivealertType.value ? this.loadAlertSummary() : this.loadDeviceSummary();
                });
            });
        }else if(this.state.SelectivealertType.label === 'All Alerts' && this.state.SelectiveDeviceName.label != undefined)
        {
            var allalerts = " Alerts on ".concat(this.state.SelectiveDeviceName.label);
            var perday = " Alerts on ".concat(this.state.SelectiveDeviceName.label)+' per day';
            this.setState({
                spinner: true,
                displaytext : allalerts,
                perday:perday,
                showResults: false,
            },()=>{
                this.loadAlerts(null, () => {          
                    this.state.SelectivealertType.value ? this.loadAlertSummary() : this.loadDeviceSummary();
                });
            });
        }else if(this.state.SelectiveDeviceName.label === 'All Devices' &&  this.state.SelectivealertType.label != undefined){
            
            var alldevice = this.state.SelectivealertType.label.concat(" Alerts on ");
                
                var allalerts = alldevice.concat(this.state.SelectiveDeviceName.label);
                var perday =this.state.SelectivealertType.label+ " Alerts on ".concat(this.state.SelectiveDeviceName.label)+' per day';
                this.setState({
                    spinner: true,
                    displaytext : allalerts,
                    perday:perday,
                    showResults: false,
                },()=>{
                    this.loadAlerts(null, () => {          
                        this.state.SelectivealertType.value ? this.loadAlertSummary() : this.loadDeviceSummary();
                    });
                })   
        }
        else{

            this.setState({
                spinner: true,
            },()=>{
                this.loadAlerts(null, () => {          
                    this.state.SelectivealertType.value ? this.loadAlertSummary() : this.loadDeviceSummary();
                });
            });
        }      
    }

    loadAlerts = (customDate, callback) => {
        let selectedDevicename = this.state.SelectiveDeviceName.value
        let selectedAlertType = this.state.SelectivealertType.value

        let api;
        let dateFrom = Utils.getLocalYYYYMMDD(this.state.startDate);
        let dateTo = Utils.getLocalYYYYMMDD(this.state.endDate);
        
        let device = selectedDevicename ? Utils.getDeviceName(selectedDevicename) :  "All Devices"
        let alert = selectedAlertType ? alertTypes[selectedAlertType] : "All Alerts"
        // Event(
        //     "AlertsAnalytics",
        //     "AnalyzeAlerts",
        //     device + ' '+alert,
        //     Utils.diffInDates(dateFrom)
        // )

        if( selectedDevicename != undefined && selectedAlertType == undefined )
        {    
             api = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/' + Utils.getSite() + '/alerts/?device=' + selectedDevicename + '&dateFrom=' + dateFrom + '&dateTo=' + dateTo
        }else if(selectedDevicename == undefined && selectedAlertType != undefined)
        {               
            api = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/' + Utils.getSite() + '/alerts/?type=' + selectedAlertType + '&dateFrom=' + dateFrom + '&dateTo=' + dateTo
        }else{               
            api = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/' + Utils.getSite() + '/alerts/?device=' + selectedDevicename + '&type=' + selectedAlertType + '&dateFrom=' + dateFrom + '&dateTo=' + dateTo              
        }

        axios.get(api, { withCredentials: true })
            .then(res => {
                res.data.forEach((alert)=>{
                    alert.type = alertTypes[alert.type];
                    alert.device = Utils.getDeviceName(alert.device);           
                })

                this.setState({
                    spinner: true,
                    data: res.data
                })
                this.loadChart()
                callback();
            })
            .catch(err => {
                callback();
                this.setState({
                    data: [],
                })
                // Toaster({ title: 'Error', message: 'Error While Retrieving Alerts' }, 'error')

            })
    }

    groupBy(alerts, paramter) {
        
        var groupMap = [];
        let parameterMapCount = {};
        let groupAlerts ={};
        alerts.forEach(function (alert) {
            parameterMapCount[alert[paramter]] = (parameterMapCount[alert[paramter]] || 0) + 1;
            groupAlerts[alert[paramter]] = groupAlerts[alert[paramter]]? groupAlerts[alert[paramter]].concat(alert) : [alert];

        });

        for (let key in parameterMapCount) {
            groupMap.push([key, parameterMapCount[key],groupAlerts[key]]);
        }
        return groupMap;
    }

    loadDeviceSummary = () => {

        var deviceAlerts = this.groupBy(this.state.data,'type');    
        var deviceAlertsChart = [];
        var dateAlertsChart = []; 
        var deviceAlertDuration = []; 
     
        deviceAlerts.forEach( (typeGroup) =>{

            deviceAlertsChart.push({
                name: typeGroup[0],
                y: typeGroup[1],
                drilldown : typeGroup[0]                                    
            });
            
            let typeGroupDuration =this.averageDuration(typeGroup[2]);

            deviceAlertDuration.push({
                name:typeGroup[0],
                y: typeGroupDuration,
                drilldown: typeGroup[0]+"MEAN"
            })

            let dateAlertCount = this.alertCountByDate(typeGroup[2])


            let dateAlertDuration = [];
            dateAlertCount.forEach((dateAlert)=>{
                dateAlertDuration.push([dateAlert[0],this.averageDuration(dateAlert[2])])
            })

            dateAlertsChart.push({
                xAxis:1,
                data :this.alertCountByDate(typeGroup[2]),
                id: typeGroup[0],
                name: 'Alerts'
            },{
                name: 'Alerts Avg Duration (minutes)',
                data : dateAlertDuration,
                type: 'spline',
                tooltip: {
                    valueDecimals: 2,
                    xDateFormat: '%d-%m-%Y',
                    useHTML:true
                },
                id: typeGroup[0]+"MEAN",
                xAxis:1,
                yAxis:1,
            });         
        })


        let deviceOptions = {
            chart: {
                type: 'column',
                zoomType: 'x',
                events: {
                    drilldown: function() {
                    var chart = this;

                    var H = Highcharts;

                    H.Chart.prototype.protoEvents.beforeShowResetZoom[0].order = 1;

                    H.addEvent(
                    H.Chart,
                    'beforeShowResetZoom',
                    function(event) {
                        if (this.drillUpButton) {
                        this.temporaryDrillUpButton = this.drillUpButton;
                        delete this.drillUpButton;
                        }
                    }, {
                        order: 0
                    }
                    );

                    H.addEvent(
                    H.Chart,
                    'afterShowResetZoom',
                    function(event) {
                        if (this.temporaryDrillUpButton && this.resetZoomButton) {
                        var buttonOptions = this.options.drilldown.drillUpButton,
                            bbox = this.resetZoomButton.getBBox();

                        this.temporaryDrillUpButton.align({
                            x: buttonOptions.position.x - bbox.width - 20,
                            y: buttonOptions.position.y,
                            align: 'right'
                            },
                            false,
                            buttonOptions.relativeTo || 'plotBox'
                        );


                        this.drillUpButton = this.temporaryDrillUpButton;
                        delete this.temporaryDrillUpButton;
                        }
                    }
                    );

                       
                        this.xAxis[1].update({	
                            labels: {	
                              enabled: true	
                            },	
                            type: 'datetime',	
                          }, false);
                        chart.setTitle(null, {
                          text: "Total alerts per day"
                        });
                        
                    },
                    drillup: function() {
                        var chart = this;  
                        
                        var H = Highcharts;

                        H.Chart.prototype.protoEvents.beforeShowResetZoom[0].order = 1;

                        H.addEvent(
                        H.Chart,
                        'beforeShowResetZoom',
                        function(event) {
                            if (this.drillUpButton) {
                            this.temporaryDrillUpButton = this.drillUpButton;
                            delete this.drillUpButton;
                            }
                        }, {
                            order: 0
                        }
                        );
    
                        H.addEvent(
                        H.Chart,
                        'afterShowResetZoom',
                        function(event) {
                            if (this.temporaryDrillUpButton && this.resetZoomButton) {
                            var buttonOptions = this.options.drilldown.drillUpButton,
                                bbox = this.resetZoomButton.getBBox();
    
                            this.temporaryDrillUpButton.align({
                                x: buttonOptions.position.x - bbox.width - 20,
                                y: buttonOptions.position.y,
                                align: 'right'
                                },
                                false,
                                buttonOptions.relativeTo || 'plotBox'
                            );
    
    
                            this.drillUpButton = this.temporaryDrillUpButton;
                            delete this.temporaryDrillUpButton;
                            }
                        }
                        );
    
                        if (this.resetZoomButton) {
                            chart.resetZoomButton.hide();
                        }
                       
                        this.xAxis[1].update({	
                            labels: {	
                              enabled: false	
                            },	
                            type: 'category',	
                          }, false);            
                        chart.setTitle(null, {
                          text: ""
                        });
                       
                    }
                }
            },
            title: {
                text: this.state.displaytext
            },
            xAxis: [{
                id: 0,
                type: 'category',
                showLastLabel: true,
                gridLineWidth: 0,
              }, {
                id: 1,
                type: 'datetime',
                showLastLabel: true,
                labels: {
                    format: '{value:%e-%b-%y}'
                  },
                lineColor: 'transparent',
                gridLineWidth: 0,
              }],
            tooltip: {
                shared: true,
                useHTML: true
            },
            yAxis:[
                {
                    title: {
                        text: 'Count',
                    },
                    labels: {
                        format: '{value}',
                     
                    }
                }, {
                    title: {
                        text: 'Time(Minutes)',
                    },
                    labels: {
                        format: '{value}',
                      
                    },
                    opposite: true,
                    min: 0
                }
            ],
            legend: {
                enabled: true
            },
            credits: {
                enabled: false
              },
            plotOptions: {
                series: {
                    stacking: 'normal',
                },
                column: {
                    maxPointWidth: 50
                }
                
            },
            series: [
                {
                    name: 'Alerts',
                    data: deviceAlertsChart,
                    turboThreshold: 0,
                    cropThreshold: 500,
                },
                {
                    name: 'Alerts Avg Duration (minutes)',
                    data : deviceAlertDuration,yAxis: 1,
                    type: 'spline',
                    tooltip: {
                        valueDecimals: 2,
                        xDateFormat: '%d-%m-%Y',
                        useHTML:true
                    }
                }
            ],
            drilldown:{
                allowPointDrilldown: false,
                series: dateAlertsChart,       
                activeAxisLabelStyle: {
                    textDecoration: 'none',
                    color: '#808080'             
                },             
            },
        
            exporting: {
                filename: 'Alerts Analytics - '+this.state.displaytext+' - From '+ moment(this.state.startDate).format('DD-MMM-YYYY')+' To '+moment(this.state.endDate).format('DD-MMM-YYYY'),
                chartOptions: {
                    title: {
                        text: 'Alerts Analytics - '+this.state.displaytext+', '+Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']
                    }
                }
            },
        }
        this.setState({
            spinner: false,
            deviceAndAlertChartOptions: deviceOptions,
        })
    }

    loadAlertSummary() {
        var alertsCount = this.groupBy(this.state.data,'device');
        var deviceAlertsChart = [];
        var dateAlertsChart = [];  
        var deviceAlertDuration = [];   

        alertsCount.forEach(typeGroup=>{ 
   
            deviceAlertsChart.push({
                name: typeGroup[0],
                y: typeGroup[1],
                drilldown : typeGroup[0],                            
            });

            let typeGroupDuration =this.averageDuration(typeGroup[2]);

            deviceAlertDuration.push({
                name: typeGroup[0],
                y: typeGroupDuration,
                drilldown : typeGroup[0]+"MEAN" 
            })

            let dateAlertCount = this.alertCountByDate(typeGroup[2])
            let dateAlertDuration = [];

            dateAlertCount.forEach((dateAlert)=>{
                dateAlertDuration.push([dateAlert[0],this.averageDuration(dateAlert[2])])
            })
        
            dateAlertsChart.push({
                xAxis: 1,
                data :this.alertCountByDate(typeGroup[2]),
                id: typeGroup[0],
                name: 'Alerts',
            },{
                name: 'Alerts Avg Duration (minutes)',
                data : dateAlertDuration,
                type: 'spline',
                tooltip: {
                    valueDecimals: 2,
                    xDateFormat: '%d-%m-%Y',
                    useHTML:true
                },
                id: typeGroup[0]+"MEAN",
                xAxis: 1,
                yAxis: 1
            });
        })


        let alertOptions = { 
            chart: {
                type: 'column',
                zoomType: 'x',   
                       
                events: {

                    drilldown: function() {

                    var chart = this;

                    var H = Highcharts;

                    H.Chart.prototype.protoEvents.beforeShowResetZoom[0].order = 1;

                    H.addEvent(
                    H.Chart,
                    'beforeShowResetZoom',
                    function(event) {
                        if (this.drillUpButton) {
                        this.temporaryDrillUpButton = this.drillUpButton;
                        delete this.drillUpButton;
                        }
                    }, {
                        order: 0
                    }
                    );

                    H.addEvent(
                    H.Chart,
                    'afterShowResetZoom',
                    function(event) {
                        if (this.temporaryDrillUpButton && this.resetZoomButton) {
                        var buttonOptions = this.options.drilldown.drillUpButton,
                            bbox = this.resetZoomButton.getBBox();

                        this.temporaryDrillUpButton.align({
                            x: buttonOptions.position.x - bbox.width - 20,
                            y: buttonOptions.position.y,
                            align: 'right'
                            },
                            false,
                            buttonOptions.relativeTo || 'plotBox'
                        );


                        this.drillUpButton = this.temporaryDrillUpButton;
                        delete this.temporaryDrillUpButton;
                        }
                    }
                    );

                      this.xAxis[1].update({	
                        labels: {	
                          enabled: true	
                        },	
                        type: 'datetime',	
                      }, false);
                      chart.setTitle(null, {
                        text: "Total alerts per day"
                      });
                      
                    },
                    drillup: function() {
                        
                    var chart = this;
                    
                    var H = Highcharts;

                    var H = Highcharts;

                    H.Chart.prototype.protoEvents.beforeShowResetZoom[0].order = 1;

                    H.addEvent(
                    H.Chart,
                    'beforeShowResetZoom',
                    function(event) {
                        if (this.drillUpButton) {
                        this.temporaryDrillUpButton = this.drillUpButton;
                        delete this.drillUpButton;
                        }
                    }, {
                        order: 0
                    }
                    );

                    H.addEvent(
                    H.Chart,
                    'afterShowResetZoom',
                    function(event) {
                        if (this.temporaryDrillUpButton && this.resetZoomButton) {
                        var buttonOptions = this.options.drilldown.drillUpButton,
                            bbox = this.resetZoomButton.getBBox();

                        this.temporaryDrillUpButton.align({
                            x: buttonOptions.position.x - bbox.width - 20,
                            y: buttonOptions.position.y,
                            align: 'right'
                            },
                            false,
                            buttonOptions.relativeTo || 'plotBox'
                        );


                        this.drillUpButton = this.temporaryDrillUpButton;
                        delete this.temporaryDrillUpButton;
                        }
                    }
                    );

                      if (this.resetZoomButton) {
                        chart.resetZoomButton.hide();
                      }
                                
                      this.xAxis[1].update({	
                        labels: {	
                          enabled: false	
                        },	
                        type: 'category',	
                      }, false);
                      
                      chart.setTitle(null, {
                        text: ""
                      });
                     
                    }
                },
            },
            title: {
                text: this.state.displaytext
            },       
            xAxis: [{
                id: 0,
                type: 'category',
                showLastLabel: true,   
                uniqueNames: false,
                minorGridLineWidth:0,
                gridLineWidth: 0
                
              }, {
                id:1,
                type: 'datetime',
                labels: {
                    format: '{value:%e-%b-%y}'
                },
                lineColor: 'transparent',
                uniqueNames: false,      
                minorGridLineWidth:0,   
                gridLineWidth: 0,
                tickPixelInterval: 150,
                showLastLabel: true
              }],

            tooltip: {
                shared: true,
                useHTML: true
            },
            yAxis:[
                {
                    title: {
                        text: 'Count',
                    },
                    labels: {
                        format: '{value}',
                     
                    }
                }, {
                    title: {
                        text: 'Time(Minutes)',
                    },
                    labels: {
                        format: '{value}',
                      
                    },
                    opposite: true,
                    min: 0
                }
            ],
            legend: {
                enabled: true
            },
            credits: {
                enabled: false
              },
            plotOptions: {
                series: {
                    stacking: 'normal',
                },
                column: {
                    maxPointWidth: 50
                }
            },
            series: [
                {
                    name: 'Alerts',
                    data: deviceAlertsChart,
                    turboThreshold: 0,
                    cropThreshold: 500,               
                },
                {
                    name: 'Alerts Avg Duration (minutes)',
                    data : deviceAlertDuration,yAxis: 1,
                    type: 'spline',        
                    tooltip: {
                        valueDecimals: 2,
                        xDateFormat: '%d-%m-%Y',
                        useHTML:true
                    }
                }
            ],
            drilldown:{
                allowPointDrilldown: false,
                series: dateAlertsChart,   
                activeAxisLabelStyle: {
                    textDecoration: 'none',
                    color: '#808080'             
                }
            },
        
            exporting: {
                filename: 'Alerts Analytics - '+this.state.displaytext+ ' - '+' - From '+ moment(this.state.startDate).format('DD-MMM-YYYY')+' To '+moment(this.state.endDate).format('DD-MMM-YYYY'),
                chartOptions: {
                    title: {
                        text: 'Alerts Analytics - '+this.state.displaytext+', '+Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']
                    }
                }
            },
        }
        this.setState({
            spinner: false,
            deviceAndAlertChartOptions: alertOptions, 
        })
    }

    alertCountByDate(data)
    {       
        var alertsCount = []
        let dateAlertCount = {}; 
        let dualdata = {};  
        data.map(function (count) {         
            dateAlertCount[moment(count.openTS).format('DD/MM/YYYY')] = (dateAlertCount[moment(count.openTS).format('DD/MM/YYYY')] || 0) + 1;  
            dualdata[moment(count.openTS).format('DD/MM/YYYY')] = dualdata[moment(count.openTS).format('DD/MM/YYYY')]? dualdata[moment(count.openTS).format('DD/MM/YYYY')].concat(count) : [count];                       
        });              
        for(let date in dateAlertCount) {
            alertsCount.push([moment(date,"DD/MM/YYYY").toDate().getTime(),dateAlertCount[date],dualdata[date]])            
        }
        return alertsCount;       
    }

    averageDuration(alerts){
        
        let sumDuration = 0;
        let closedAlertCount = 0
        alerts.forEach((alert) => {
            if(alert.isOpen === false){
                let duration =  moment.duration(moment(alert.closeTS).diff(moment(alert.openTS)));
                sumDuration += duration.asMinutes() ;
                closedAlertCount += 1
            }
            else if(alert.isOpen === true)
            {     
                let duration =  moment.duration(moment(moment(Date.now())).diff(moment(alert.openTS)));
                sumDuration += duration.asMinutes() ;
                closedAlertCount += 1
            }
        });
        let avgDuration = sumDuration / closedAlertCount;
        return avgDuration;  
    }

    loadChart = () =>
    {   
        let dayAlertCount =  this.alertCountByDate(this.state.data);
        var avgDuration = [];       
        dayAlertCount.map( (dataValue) => {
            avgDuration.push([dataValue[0],this.averageDuration(dataValue[2])])        
        });
        let deviceAlertOptions = {
            chart: {
                type: 'column',
                zoomType: 'x',
            },
            title: {
                text: this.state.perday
            },
            xAxis: {
                title: {	
                    enabled: true,	
                    text: ''	
                },
                type: 'datetime',
                labels: {
                    step: 1 ,
                    format: '{value:%e-%b-%y}'
                    
                },
                tickPixelInterval: 150,
                showLastLabel: true   
              },
            tooltip: {
                shared: true,    
                useHTML:true    
                // xDateFormat: '%d-%m-%Y'   
            },
            yAxis:[
                {
                    title: {
                        text: 'count',
                    },
                    labels: {
                        format: '{value}',
                     
                    }
                }, {
                    title: {
                        text: 'Time(Minutes)',
                    },
                    labels: {
                        format: '{value}',
                      
                    },
                    opposite: true,
                    min: 0
                }
            ],
            legend: {
                enabled: true
            },
            credits: {
                enabled: false
              },
            plotOptions: {
                series: {
                    stacking: 'normal',
                },
                column: {
                    maxPointWidth: 50
                }
            },
            series: [
                {
                    name: 'Alerts',
                    data: dayAlertCount,
                    
                },
                {
                    name: 'Alerts Avg Duration (minutes)',
                    data : avgDuration, yAxis: 1,
                    type: 'spline',
                    tooltip: {
                        valueDecimals: 2,
                        useHTML:true
                        // xDateFormat: '%d-%m-%Y'             
                    }
                }
            ],
            exporting: {
              
                filename: 'Alerts Analytics  - '+this.state.perday+' - From '+' - From '+ moment(this.state.startDate).format('DD-MMM-YYYY')+' To '+moment(this.state.endDate).format('DD-MMM-YYYY'),
                chartOptions: {
                    title: {
                        text: 'Alerts Analytics - '+this.state.perday+', '+Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']
                    }
                }
            },
        }
        this.setState({
            barOptionsDeviceAlerts: deviceAlertOptions
        })    

    }

    render() {
        const {deviceOptions,alertOptions,plantName,ranges,spinner,SelectiveDeviceName, SelectivealertType, showResults, startDate, endDate, maxDate} = this.state;

        return (
            <Aux>
                <div className="page-header">
                    <div className="page-block">
                        <Row className="align-items-center">
                            <Col md={12} lg={12}>
                                <div className="page-header-title m-t-30">
                                    <h5 className="m-b-10 float-left">Alerts Analytics</h5>
                                    {/* <Button variant='success' onClick={this.onSubmit} className='m-0 btn-sm mt-0 float-right'>Submit</Button> */}
                                    <DateRangePicker
                                        startDate={startDate}
                                        endDate={endDate}
                                        minDate='01/01/2012'
                                        maxDate={maxDate}
                                        showWeekNumbers={false}
                                        opens={'left'}
                                        showDropdowns={true}
                                        onApply={this.handleCustomDate}
                                        ranges={ranges}
                                        containerClass={'float-right m-r-10'}
                                        timezone= {moment.tz.setDefault("Asia/Kolkata")} //{( Utils.getSiteMetaData(Utils.getSite()) && Utils.getSiteMetaData(Utils.getSite())["timeZone"] )? moment.tz.setDefault(Utils.getSiteMetaData(Utils.getSite())["timeZone"]):moment.tz.setDefault("Asia/Kolkata")}
                                        >
                                        <Button  className='m-0 btn-sm mt-0 float-right'>
                                            <i className={'feather icon-calendar'} />
                                            {moment(this.state.startDate).format('DD-MM-YYYY') === moment(this.state.endDate).format('DD-MM-YYYY') ? moment(this.state.startDate).format('DD-MMM-YYYY') : 'From ' + moment(this.state.startDate).format('DD-MMM-YYYY') + ' To ' + moment(this.state.endDate).format('DD-MMM-YYYY')} <b className="fa fa-caret-down ml-2"></b>
                                        </Button>
                                    </DateRangePicker>
                                    <div className="col-md-3 col-xs-12 float-right">
                                        <Select
                                            onChange={this.handleAlertType}
                                            value={SelectivealertType}
                                            name="alert"
                                            options={alertOptions}
                                        />
                                    </div>
                                    <div className="col-md-3 col-xs-12 float-right">
                                        <Select
                                            onChange={this.handleDevice}
                                            value={SelectiveDeviceName}
                                            name="deviceName"
                                            options={this.state.deviceOptions}
                                        />
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Breadcrumb className='mr-auto'>
                                <Breadcrumb.Item href={'#'}><i className="feather icon-home" /></Breadcrumb.Item>
                                {/* <Breadcrumb.Item href={'#'}>
                                    <Link to={"/" + Utils.getSite() + '/dashboard'}>
                                        {plantName}
                                    </Link>
                                </Breadcrumb.Item>
                                <Breadcrumb.Item href={'#'}>
                                    <Link to={ '/alerts/dashboard'}>
                                        Alerts Dashboard
                                    </Link>
                                </Breadcrumb.Item> */}
                                <Breadcrumb.Item active>Alerts Analytics</Breadcrumb.Item>
                            </Breadcrumb>
                        </Row>
                    </div>
                </div>
                <Row>

                        <Col sm={12}>
                            <Card>
                                <Card.Header>
                                    <Card.Title as='h5'>Network Switch Alerts Zone wise</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                        {
                                                false ?
                                                (<div className="text-center">
                                                    <Spinner />
                                                </div>) : (
                                                <Chart chartConfig={firstChart} darkenColors={true} style={{ style: { height: "400px" }}}date={moment(this.state.startDate).format('DD-MM-YYYY') == moment(this.state.endDate).format('DD-MM-YYYY') ? moment(this.state.startDate).format('DD-MMM-YYYY'): 'From '+ moment(this.state.startDate).format('DD-MMM-YYYY')+' To '+moment(this.state.endDate).format('DD-MMM-YYYY')} 
                                                // plantDisplayName={Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']} 
                                                chartTitle={this.state.displaytext+' - Alerts Analytics'}/>
                                                )
                                            }
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    <Row>

                        <Col sm={12}>
                            <Card>
                                <Card.Header>
                                    <Card.Title as='h5'>Network Switch Alerts in all Zones per day</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                        {
                                                false ?
                                                (<div className="text-center">
                                                    <Spinner />
                                                </div>) : (
                                                <Chart chartConfig={secondChart} darkenColors={true} style={{ style: { height: "400px" }}}date={moment(this.state.startDate).format('DD-MM-YYYY') == moment(this.state.endDate).format('DD-MM-YYYY') ? moment(this.state.startDate).format('DD-MMM-YYYY'): 'From '+ moment(this.state.startDate).format('DD-MMM-YYYY')+' To '+moment(this.state.endDate).format('DD-MMM-YYYY')} chartTitle={this.state.displaytext+' - Alerts Analytics'}/>
                                                )
                                            }
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>                                            
                {/* {
                showResults  ?
                (<Row>

                        <Col sm={12}>
                            <Card>
                                <Card.Header>
                                    <Card.Title as='h5'>{ this.state.displaytext } </Card.Title>
                                </Card.Header>
                                <Card.Body>
                                        {
                                                spinner ?
                                                (<div className="text-center">
                                                    <Spinner />
                                                </div>) : (
                                                <Chart chartConfig={this.state.barOptionsDeviceAlerts} darkenColors={true} style={{ style: { height: "400px" }}}date={moment(this.state.startDate).format('DD-MM-YYYY') == moment(this.state.endDate).format('DD-MM-YYYY') ? moment(this.state.startDate).format('DD-MMM-YYYY'): 'From '+ moment(this.state.startDate).format('DD-MMM-YYYY')+' To '+moment(this.state.endDate).format('DD-MMM-YYYY')} plantDisplayName={Utils.getSiteMetaData(Utils.getSite())['plantDisplayName']}chartTitle={this.state.displaytext+' - Alerts Analytics'}/>
                                                )
                                            }
                                </Card.Body>
                            </Card>
                        </Col>
                        </Row>) : 
                (<Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as='h5'> { this.state.displaytext }{this.state.displaytext!=''?SelectiveDeviceName.value!=null?(
                                <a onClick={() => this.toMonitorInverter(SelectiveDeviceName,this.state.startDate,this.state.endDate)} className='f-12 label label-info m-l-10 text-white' title='Inverter Dashboard'><i className="fa fa-line-chart"></i></a>) :'':''} </Card.Title>
                            </Card.Header>
                            <Card.Body>
                                {
                                    spinner ?
                                    (<div className="text-center">
                                        <Spinner />
                                    </div>) : (
                                    <Chart  chartConfig={this.state.deviceAndAlertChartOptions} darkenColors={true} style={{ style: { height: "400px" }}}/>
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as='h5'>{ this.state.perday }</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                {
                                    spinner ?
                                    (<div className="text-center">
                                        <Spinner />
                                    </div>) : (
                                    <Chart chartConfig={this.state.barOptionsDeviceAlerts} darkenColors={true} style={{ style: { height: "400px" }}}/>
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row> )
                } */}
            </Aux>
        )
    }
}

export default AlertsAnalytics