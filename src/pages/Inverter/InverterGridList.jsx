import React, { Component } from 'react';
import SingleInverter from './SingleInverter';
import Utils from "../../Utils";
import { Row, Col } from 'react-bootstrap';

class InverterGridList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            InverterColors:null
        }
    }

    componentDidMount() {

        let InverterColors = {
            inActive : 0,
            green : 0,
            lightgreen : 0,
            yellow : 0,
            lightyellow : 0,
            lightorange : 0,
            exclamatory : 0
        }
        const { selectedDate, Inverters } = this.props
        Object.keys(Inverters).map((invId,index) => {
            if (selectedDate.toDateString() === new Date().toDateString()) {
                let commTimeoutMins = 10 // Utils.getSiteMetaData(Utils.getSite())["commTimeoutMins"] || 10;
                if(Inverters[invId].acPower <= 0) { InverterColors.exclamatory +=1 }
                // if (!Inverters[invId] || (Date.now() - new Date(Inverters[invId].ts) >= Math.floor(commTimeoutMins * 60 * 1000))){   
                //     InverterColors.inActive += 1
                // }
                // else 
                if (Inverters[invId].PR == 100){   
                    InverterColors.inActive += 1
                }
                else if (Inverters[invId].deviation > 1) { InverterColors.green += 1 }
                else if (Inverters[invId].deviation >= 0 && Inverters[invId].deviation <= 1) {InverterColors.lightgreen += 1 } 
                else if (Inverters[invId].deviation >= -1 && Inverters[invId].deviation <= 0) { InverterColors.yellow += 1 } 
                else if (Inverters[invId].deviation >= -2 && Inverters[invId].deviation <= -1) { InverterColors.lightyellow += 1 }
                else if (Inverters[invId].deviation <= -2) {  InverterColors.lightorange += 1 }              
            }else{
                if (Inverters[invId].deviation > 1) {  
                    InverterColors.green +=1}
                else if (Inverters[invId].deviation >= 0 && Inverters[invId].deviation <= 1) {
                    InverterColors.lightgreen += 1}
                else if (Inverters[invId].deviation >= -1 && Inverters[invId].deviation <= 0) {
                    InverterColors.yellow += 1}
                else if (Inverters[invId].deviation >= -2 && Inverters[invId].deviation <= -1) {
                    InverterColors.lightyellow += 1}
                else if (Inverters[invId].deviation <= -2) { 
                    InverterColors.lightorange +=1}
            }
        })
        this.setState({ InverterColors: InverterColors });
    }

    render() {
        const { Inverters } = this.props;
        let InverterColors = {
            inActive : 0,
            green : 0,
            lightgreen : 0,
            yellow : 0,
            lightyellow : 0,
            lightorange : 0,
            exclamatory : 0
        }
        const { selectedDate } = this.props
        Object.keys(Inverters).map((invId,index) => {
            if (selectedDate.toDateString() === new Date().toDateString()) {
            // if (new Date().toDateString() === new Date().toDateString()) {
                let commTimeoutMins =  10 // Utils.getSiteMetaData(Utils.getSite())["commTimeoutMins"] || 10;
                if(Inverters[invId].ACPower <= 0) { InverterColors.exclamatory +=1 }
                // if (!Inverters[invId] || (Date.now() - new Date(Inverters[invId].ts) >= Math.floor(commTimeoutMins * 60 * 1000))){   
                //     InverterColors.inActive += 1
                // } 
                // else 
                if (Inverters[invId].PR == 100){   
                    InverterColors.inActive += 1
                } 
                else if (Inverters[invId].deviation > 1) { InverterColors.green += 1 }
                else if (Inverters[invId].deviation >= 0 && Inverters[invId].deviation <= 1) {InverterColors.lightgreen += 1 } 
                else if (Inverters[invId].deviation >= -1 && Inverters[invId].deviation <= 0) { InverterColors.yellow += 1 } 
                else if (Inverters[invId].deviation >= -2 && Inverters[invId].deviation <= -1) { InverterColors.lightyellow += 1 }
                else if (Inverters[invId].deviation <= -2) {  InverterColors.lightorange += 1 } 
                                
            }else{
                if (Inverters[invId].deviation > 1) {  
                    InverterColors.green +=1}
                else if (Inverters[invId].deviation >= 0 && Inverters[invId].deviation <= 1) {
                    InverterColors.lightgreen += 1}
                else if (Inverters[invId].deviation >= -1 && Inverters[invId].deviation <= 0) {
                    InverterColors.yellow += 1}
                else if (Inverters[invId].deviation >= -2 && Inverters[invId].deviation <= -1) {
                    InverterColors.lightyellow += 1}
                else if (Inverters[invId].deviation <= -2) { 
                    InverterColors.lightorange +=1}
            }
        })
        if (!Inverters) {
            return (
                <div className="text-center">
                    <p className="m-0">
                        <i style={{ color: "#000000" }} className="fa fa-exclamation-circle fa-20" /> 
                        No Inverters are configured for this site
                    </p>
                </div>
            )
        } else {
            return (
                <Row>
                    <Col md={12} className="mb-2">
                        { InverterColors ? (
                            <div className="float-right">
                                <div className='float-left f-12'>
                                    <div style={{margin: '0 4px 0 10px'}} className="legend-pill float-left green"></div>
                                    {InverterColors.green}
                                </div>
                                <div className='float-left f-12'>
                                    <div style={{margin: '0 4px 0 10px'}} className="legend-pill float-left light-green"></div>
                                    {InverterColors.lightgreen}
                                </div>
                                <div className='float-left f-12'>
                                    <div style={{margin: '0 4px 0 10px'}} className="legend-pill float-left yellow"></div>
                                    {InverterColors.yellow}
                                </div>
                                <div className='float-left f-12'>
                                    <div style={{margin: '0 4px 0 10px'}} className="legend-pill float-left light-yellow"></div>
                                    {InverterColors.lightyellow}
                                </div>
                                <div className='float-left f-12'>
                                    <div style={{margin: '0 4px 0 10px'}} className="legend-pill float-left light-orange"></div>
                                    {InverterColors.lightorange}
                                </div>
                                <div className='float-left f-12'>
                                    <div style={{margin: '0 4px 0 10px'}} className="legend-pill float-left inActive"></div>
                                    {InverterColors.inActive}
                                </div>
                                <div className='float-left f-18'>
                                    <div style={{ margin: '0 4px 0 10px' }} className="d-flex align-items-center f-20">
                                        <i className="legend-pill fa fa-exclamation-circle" />
                                        <span className="f-12 ml-1">{InverterColors.exclamatory}</span>
                                    </div>
                                </div>
                            </div>
                        ) : null }
                    </Col>
                    <Col md={12}>
                        { Object.keys(Inverters).map((el, index) => (
                            <SingleInverter elementAction={this.props.elementAction} key={index} element={Inverters[el]} selectedDate={this.props.selectedDate} />
                        ) )}
                    </Col>
                </Row>
            );
        }
    }
}

export default InverterGridList;