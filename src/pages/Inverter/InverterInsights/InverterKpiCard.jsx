import React, { Component } from 'react';
import "bootstrap-daterangepicker/daterangepicker.css";
import Chart from '../../Alerts/Chart'
import Utils from "../../../Utils";
import moment from 'moment';
import { Link } from "react-router-dom";
import { Row, Col,Card,Table } from 'react-bootstrap';
import alias from './aliases.json';

let devicesList = require('./devicesList.json');
class InverterKpiCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartConfig: {},
            deviceName: '',
            summary: "1",//Utils.getAggregationTime() || "1",
            startDate: moment(new Date()).toDate(),
            maxDate: moment(new Date()).toDate(),
            inverterDevices: devicesList//Utils.getInverterdevices(),
        }
        this.toAlertsAnalytics = this.toAlertsAnalytics.bind(this);
    }
    toAlertsAnalytics(device){
        let path = "/"+ Utils.getSite()+"/alerts/analytics";
        let deviceData = {"device":device}
        this.props.history.push({pathname:path,data:deviceData})
    }
    componentDidMount() {
        let { InverterKPI ,chartHeight} = this.props;
        let dataPR = []
        let dataYield = []
        let dataDowntime = []
        let weightedEfficiency = []

        InverterKPI.inverterKpiRecords.forEach(function (item) {
            let unixTime = new Date(item['date']).getTime()
            dataPR.push({'x':unixTime, 'y':(item.PR || item.PR == 0) && Utils.round(item.PR,3)})
            dataYield.push({'x':unixTime, 'y':(item.yield || item.yield == 0) && Utils.round(item.yield,3)})
            dataDowntime.push({'x':unixTime, 'y':(item.downtime || item.downtime == 0) && Utils.round(item.downtime,3)})
            weightedEfficiency.push({'x':unixTime, 'y':(item.irrEfficiency|| item.irrEfficiency == 0) && Utils.round(item.irrEfficiency,3)})
        })
       
        let chartConfig={
            chart: {
                height:chartHeight,
                zoomType: 'x',
                resetZoomButton: {
                    position: {
                        align: 'left', // right by default
                        verticalAlign: 'top', 
                        x: 10,
                        y: 10
                    },
                    relativeTo: 'chart'
                }
            },
            title:{
                text:''
            },
            xAxis:{
                type: 'datetime',               
            },
            yAxis: [{ // Secondary yAxis 0
                labels: {
                    format: '{value}',
                    style: {
                        color: '#41dd3e'
                    }
                },
                title: {
                    text: '',
                },

            }, { // Tertiary yAxis 1
                    labels: {
                        format: '{value}',
                        style: {
                            color: '#ff9f1a'
                        }
                    },
                    title: {
                        text: '',
                    },
                    opposite: true
            }, { // Tertiary yAxis 1
                labels: {
                    format: '{value}',
                    style: {
                        color: '#ff3838'
                    }
                },
                title: {
                    text: '',
                },
                opposite: true
            }],
            exporting: { enabled: false },
            legend:{
                enabled: true,
                y: 0,
                x:0,
                align: 'right',
                verticalAlign: 'top',
                borderWidth: 0,
                itemDistance: 15
            },
            tooltip: {
                shared: true,
                useHTML: true,
            },
            plotOptions: {
                series: {
                    connectNulls: true
                }
            },
            series: [{
                type: 'column',
                name:'No Network (Minutes)',
                color:'#ff3838',
                data: dataDowntime,
                borderWidth: 0,
                "yAxis": 2,
            },{
                name:'4G Availability(%)',
                type: this.props.graphType == 'bar'?'column': '',
                color:'#41dd3e',
                data: dataPR,
                "yAxis": 0,
            },{
                name:'5G Availability (kWh/kWp)',
                type: this.props.graphType == 'bar'?'column': '',
                color:'#ff9f1a',
                data: dataYield,
                "yAxis": 1,
            },{
                name:'Other Availability (%)',
                type: this.props.graphType == 'bar'?'column': '',
                color:'#4b6584',
                data: weightedEfficiency,
                "yAxis": 0,
            }]
        }
        this.setState({
            chartConfig:chartConfig,
        })
    }

    render() {
        const { chartConfig} = this.state;
        const { InverterKPI,sortBykpi,cardBorder } = this.props;

        return (
            <Row>
                <Col xl={12} md={12} className='filter-bar'>
                    <Card className={'statistial-visit '+ cardBorder} >
                        <Card.Header className={ cardBorder && 'p-3'}>
                        { cardBorder ?
                        <h6 className="f-16 float-left mr-1">
                            {/* {Utils.getDeviceName(InverterKPI.device)}  */}
                            {alias[InverterKPI.device]}
                            </h6>
                        :<h5>
                            {/* {Utils.getDeviceName(InverterKPI.device)}  */}
                            {alias[InverterKPI.device]}
                        </h5>
                         }
                            <Link to={'/inverter/dashboard' + '#' +InverterKPI.device} style={{ textDecoration: 'none'}} className='label label-info' title='View Data'><i className="fa fa-line-chart"></i>
                        </Link>
                        <span onClick={() => this.toAlertsAnalytics(InverterKPI.device)} style={{padding: '3px 8px',cursor: 'pointer'}} className='label label-danger' title='Analyze Alerts'><i className="fa fa-bell-o"></i>
                        </span>
                        {sortBykpi == 'avgPR' &&
                            <h6 className="float-right">5G Availability (%) : 
                                <span className="h6"> { Utils.round(InverterKPI.avgPR,3)}</span>
                            </h6>
                            || sortBykpi == 'avgYield' &&
                            <h6 className="float-right">Average Yield (kWh/kWp) : 
                                <span className="h6"> { Utils.round(InverterKPI.avgYield,3)}</span>
                            </h6>
                            ||sortBykpi == 'downtime' &&
                            <h6 className="float-right">Total Downtime (minutes) : 
                                <span className="h6"> { InverterKPI.downtime}</span>
                            </h6>
                            ||
                            <h6 className="float-right">Weighted Efficiency (%) : 
                                <span className="h6"> { Utils.round(InverterKPI.avgEfficiency,2)}</span>
                            </h6>
                        }
                        </Card.Header>
                        <Card.Body className='p-0'>
                            <Chart chartConfig={chartConfig}></Chart>
                            <Table className='text-center text-light m-0'>
                                <tbody>
                                    <tr>
                                        {/* {sortBykpi != 'avgPR' &&
                                        <td className="p-1 greenCard">
                                            <h4 className="f-w-300 text-light">{Utils.round(InverterKPI.avgPR,3)}</h4>
                                            <small style={{'white-space': 'break-spaces'}} className="d-block">Average PR (%)</small>
                                        </td>} */}
                                        {sortBykpi != 'avgYield' &&
                                        <td className="p-1 yellowCard">
                                            <h4 className="f-w-300 text-light">{Utils.round(InverterKPI.avgYield,3)}</h4>
                                            <small style={{'white-space': 'break-spaces'}} className="d-block">5G Availability(%)</small>
                                        </td>}
                                        <td className="p-1 blueCard">
                                            <h4 className="f-w-300 text-light">{Utils.round(InverterKPI.avgKWH,3)}</h4>
                                            <small style={{'white-space': 'break-spaces'}} className="d-block">4G Availability(%)</small>
                                        </td>
                                        {/* <td className="p-1 skyblueCard">
                                            <h4 className="f-w-300 text-light">{Utils.round(InverterKPI.sumKWH,3)}</h4>
                                            <small style={{'white-space': 'break-spaces'}} className="d-block">Total Generation (kWh)</small>
                                        </td> */}
                                        {sortBykpi != 'avgEfficiency' && 
                                        <td className="p-1" style={{background: '#4b6584'}}>
                                            <h4 className="f-w-300 text-light">{Utils.round(InverterKPI.avgEfficiency,2)}</h4>
                                            <small style={{'white-space': 'break-spaces'}} className="d-block">Other Availability(%)</small>
                                        </td>}

                                        {sortBykpi != 'downtime' && 
                                        <td className="p-1 redCard">
                                            <h4 className="f-w-300 text-light">{InverterKPI.downtime}</h4>
                                            <small style={{'white-space': 'break-spaces'}} className="d-block">No Network(Minutes)</small>
                                        </td>}
                                    </tr>
                                </tbody>
                            </Table>  
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default InverterKpiCard;