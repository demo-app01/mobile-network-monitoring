import React, { Component } from 'react';
import Aux from "../../../hoc/_Aux";
import { Row, Col,Card } from 'react-bootstrap';
import moment from 'moment';
import "bootstrap-daterangepicker/daterangepicker.css";
import Utils from "../../../Utils";
import Spinner from '../../Spinner';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import axios from 'axios';
import InverterKpiCard from './InverterKpiCard'
import Tooltip from 'rc-tooltip';
// import {Event} from '../../Tracking/index';
import BreadCrumb from '../../BreadCrumb'
import Select from 'react-select';
// import Toaster from "../../../actions/Toaster";

// const createSliderWithTooltip = Slider.createSliderWithTooltip;

// const Range = createSliderWithTooltip(Slider.Range);
const KPIOptions = [
    { value: '5G Network', label: '5G Network'},
    { value: '4G Network', label: '4G Network' },
    { value: '2G Network', label: '2G Network'},
    { value: '5G Vodafone', label: '5G Vodafone'},
    { value: '5G Ooreedo', label: '5G Ooreedo'}
]

const Handle = Slider.Handle;

let insightsData = require('./InsightsData.json');
let data = require('./data.json');

const handle = (props) => {
    const { value, dragging, index, ...restProps } = props;
    return (
        <Tooltip
            prefixCls="rc-slider-tooltip"
            overlay={value}
            visible={true}
            placement="bottom"
            key={index}
        >
            <Handle value={value} {...restProps} />
        </Tooltip>
    );
};
class InverterRanking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sortBykpi: 'avgPR',
            displayCount: 5,
            data: null,
            avgKPIs: null,
            lpInverters: null,
            hpInverters: null,
            overallAvgPR:0,
            overallAvgYield:0,
            startDate: moment().startOf('month').toDate(),
            endDate: moment(new Date()).toDate(),
            maxDate: moment(new Date()).toDate(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment()],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            invertersCount: 10,
            moduleName: "Ranking",
        }
        this.handleCustomDate = this.handleCustomDate.bind(this)
    }

    componentDidMount() {
        this.loadData(() => {
            this.parseData();
        });
    }

    handleCustomDate(event, picker) {
        this.setState({
            startDate: moment(picker.startDate).toDate(),
            endDate: moment(picker.endDate).toDate(),
            data: null,
            hpInverters:null,
            lpInverters:null,
        }, () => {
            // Event(
            //     "Inverter " +  this.state.moduleName,
            //     "LoadData",
            //     "Numberofdays",
            //     Utils.diffInDates(this.state.startDate)
            // )
            this.loadData(() => {
                this.parseData();
            });
        })
    }

    handlesortBy = (event) => {
        this.setState({
            sortBykpi: event.value,
            hpInverters:null,
            lpInverters:null
        }, () => {
            Event(
                "Inverter " +  this.state.moduleName,
                "RankBy",
                this.state.sortBykpi,
                this.state.displayCount
            )
            this.pickAndSort();
        });
    };

    handledisplayCount = (event, index) => {
        this.setState({
            displayCount: event,
            hpInverters:null,
            lpInverters:null
        }, () => {
            Event(
                "Inverter " +  this.state.moduleName,
                "InverterCountChange",
                this.state.sortBykpi,
                this.state.displayCount
            )
            this.pickAndSort();
        })
    }

    loadData(callback) {
        let {moduleName,sortBykpi,displayCount}= this.state
        this.setState({
            data: data
        },()=>{
            callback();
        })
        // Event(
        //     "Inverter " +  moduleName,
        //     "LoadData",
        //     sortBykpi,
        //     displayCount
        // )
        
            
        // let API;
        // API = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/' + Utils.getSite() + '/kpi/Inverter?dateFrom='+ Utils.getLocalYYYYMMDD(this.state.startDate) +'&dateTo='+ Utils.getLocalYYYYMMDD(this.state.endDate)
        // axios.get(API, { withCredentials: true })
        //     .then(res => {
        //         this.setState({
        //             data: res.data
        //         })
        //         callback();
        //     })
        //     .catch(err => {
        //         // Toaster({ title: 'Network Error', message: 'Error While Retrieving KPIs' }, 'error')
        //         this.setState({
        //             data: [],
        //         })
        //         callback();
        //     })
    }

    parseData() {

        let groupDevices = {}
        var avgKPIs = []
        let overallSumPR = 0
        let overallSumYield = 0
        let overallPRCount =0
        let overallYieldCount =0
       
        this.state.data.forEach((inverters) => {
            groupDevices[inverters['device']] = groupDevices[inverters['device']] ? groupDevices[inverters['device']].concat([inverters]) : [inverters];
        });
            
        for (let device in groupDevices) {
            let sumPR = 0
            let sumYield = 0
            let sumKWH = 0
            let prCount=0
            let YieldCount=0
            let KWHCount=0
            let totalDowntime= 0
            let sumEfficiency = 0
            let efficiencyCount=0 
            for (var i = 0; i < groupDevices[device].length; i++) {
               
                if(groupDevices[device][i].kwhToday){
                    sumKWH += groupDevices[device][i].kwhToday;
                    KWHCount++
                }
                if(groupDevices[device][i].PR){
                    sumPR += groupDevices[device][i].PR;
                    overallSumPR += groupDevices[device][i].PR;
                    prCount++
                    overallPRCount++
                }
                if(groupDevices[device][i].yield){
                    sumYield += groupDevices[device][i].yield;
                    overallSumYield += groupDevices[device][i].yield;
                    YieldCount++
                    overallYieldCount++
                }
                if(groupDevices[device][i].downtime){
                    totalDowntime += groupDevices[device][i].downtime
                }
                if(groupDevices[device][i].irrEfficiency){
                    sumEfficiency += groupDevices[device][i].irrEfficiency
                    efficiencyCount++
                }
            }
            avgKPIs.push({ 'device': device, 'downtime':totalDowntime, 'avgPR': prCount != 0 ? (sumPR / prCount) : 0, 'avgYield':YieldCount != 0 ? (sumYield / YieldCount) : 0, 'avgKWH':KWHCount != 0 ? (sumKWH / KWHCount) : 0 , 'sumKWH': sumKWH,'avgEfficiency': efficiencyCount != 0 ? (sumEfficiency / efficiencyCount) : 0, 'inverterKpiRecords': groupDevices[device] })
        }
        this.setState({
            avgKPIs: insightsData,
            overallAvgPR : overallSumPR/overallPRCount,
            overallAvgYield : overallSumYield/overallYieldCount
        }, () => {
            this.pickAndSort()
        })

    }

    pickAndSort() {
        let sortedArray = []
        let { sortBykpi, displayCount, avgKPIs } = this.state
        sortedArray =  avgKPIs.sort(function(a, b){
            return b[sortBykpi]-a[sortBykpi]
        })
        let firstSlice = sortedArray.slice(0, displayCount);
        let lastSlice = sortedArray.slice(sortedArray.length - displayCount, sortedArray.length);
        sortBykpi == 'downtime'?
        this.setState({
            hpInverters: lastSlice.reverse(),
            lpInverters: firstSlice
        }):
        this.setState({
            hpInverters: firstSlice,
            lpInverters: lastSlice.reverse()
        })
    }

    render() {
        const { lpInverters, hpInverters, data, invertersCount,overallAvgPR ,overallAvgYield, moduleName,sortBykpi} = this.state;
        return (
            <Aux>
                <Row>
                    <Col xl={12}>
                        <BreadCrumb 
                            startDate={this.state.startDate}  
                            maxDate={this.state.maxDate} 
                            endDate={this.state.endDate}
                            handleCustomDate={this.handleCustomDate} 
                            isDeviceDropDown={false} 
                            isSingleDatePicker={false}
                            isMultiDatePicker={true}
                            ranges={this.state.ranges}
                            siteName={ ''} 
                            moduleName={"Zone Level "+ moduleName} 
                        />
                     </Col>
                </Row>
                <Row>
                    <Col md={3} xl={3}>
                        <Card className='rides-bar'>
                            <Card.Body className='sale-view p-3'>
                            {
                                true ? 
                                <div className="row d-flex align-items-center">
                                    <div className="col-auto">
                                        <i className="feather icon-percent f-30 text-white bg-success"></i>
                                    </div>
                                    <div className="col">
                                        {/* <h3 className="f-w-300">{overallAvgPR ? Utils.round(overallAvgPR,3) : ''}</h3> */}
                                        <h3 className="f-w-300">{'80.92'}</h3>
                                        <span className="d-block">5G Availability(%) </span>
                                    </div>
                                </div>:
                                <div className="text-center">
                                <Spinner />
                                </div>
                            }
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={3} xl={3}>
                        <Card className='rides-bar'>
                            <Card.Body className='sale-view p-3'>
                            {
                                true ?  <div className="row d-flex align-items-center">
                                    <div className="col-auto">
                                        <i className="fa fa-bolt f-30 text-white bg-warning"></i>
                                    </div>
                                    <div className="col">
                                        {/* <h3 className="f-w-300">{overallAvgYield ? Utils.round(overallAvgYield,3) : ''}</h3> */}
                                        <h3 className="f-w-300">4.28</h3>
                                        <span className="d-block">4G Availability(%)</span>
                                    </div>
                                </div>:
                                <div className="text-center">
                                <Spinner />
                                </div>
                            }   
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={3} xl={3}>
                        <Card>
                            <Card.Body className='p-3'>
                                <h5 className="mb-4">Rank By</h5>
                                <Select 
                                    className="basic-single"
                                    onChange={this.handlesortBy}
                                    // value={this.state.sortBykpi}
                                    defaultValue={KPIOptions[0]}
                                    data-placeholder="Select KPI"
                                    style={{ width: "100%" }}
                                    name="sortBykpi"
                                    options={KPIOptions}
                                />
                                   
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={3} xl={3}>
                        <Card>
                            <Card.Body className='p-3'>
                                <h5 className="mb-4">Number of Ranks</h5>
                                <Slider 
                                    className="pc-range-slider m-b-25" 
                                    defaultValue={5} 
                                    handle={handle}
                                    onAfterChange={this.handledisplayCount}
                                    max={invertersCount}
                                     />
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                  
                <Row>
                    <Col md={6} xl={6}>
                        <Card className='statistial-visit'>
                            <Card.Header>
                                <Card.Title as='h5'>High Performing Zones</Card.Title>
                            </Card.Header>
                            <Card.Body className='pb-0' style={{'max-height': '785px','overflow': 'auto'}}>
                            {
                                hpInverters ?
                                    hpInverters.map((invData, index) => {
                                        return <InverterKpiCard key={invData.device} cardBorder={'card-border-c-green'} InverterKPI={invData} sortBykpi={sortBykpi} chartHeight={200} history={this.props.history}/>

                                    }) : 
                                    <div className="text-center">
                                        <Spinner />
                                    </div>
                            }
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} xl={6}>
                        <Card className='statistial-visit'>
                            <Card.Header>
                                <Card.Title as='h5'>Low Performing Zones</Card.Title>
                            </Card.Header>
                            <Card.Body className='pb-0' style={{'max-height': '785px','overflow': 'auto'}}>
                            {
                                    lpInverters ?
                                    lpInverters.map((invData, index) => {
                                        return <InverterKpiCard key={invData.device} cardBorder={'card-border-c-red'} InverterKPI={invData} sortBykpi={sortBykpi} chartHeight={200} history={this.props.history}/>

                                    }) : 
                                    <div className="text-center">
                                    <Spinner />
                                </div>
                            }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
               
            </Aux>
        );
    }
}

export default InverterRanking;