import React, { Component } from 'react';
import { Row, Col,Card } from 'react-bootstrap';
import moment from 'moment';
import "bootstrap-daterangepicker/daterangepicker.css";
import Utils from "../../../Utils";
import Spinner from '../../Spinner';
import 'rc-slider/assets/index.css';
import axios from 'axios';
import InverterKpiCard from './InverterKpiCard'
import {Event} from '../../Tracking/index';
import Toaster from "../../../actions/Toaster";

class YesterdaysInvertersRanking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sortBykpi: 'avgPR',
            displayCount: 5,
            data: null,
            avgKPIs: null,
            lpInverters: null,
            hpInverters: null,
            overallAvgPR:0,
            overallAvgYield:0,
            startDate: moment(new Date()).subtract(1, 'days'),
            endDate: moment(new Date()).toDate(),
            maxDate: moment(new Date()).toDate(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment()],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            invertersCount: Utils.getInverterdevices().length,
            moduleName: "Ranking",
        }
    }

    componentDidMount() {
        this.loadData(() => {
            this.parseData();
        });
    }

    loadData(callback) {
        let {moduleName,sortBykpi,displayCount}= this.state
        Event(
            "Inverter " +  moduleName,
            "LoadData",
            sortBykpi,
            displayCount
        )
        let API;
        API = process.env.REACT_APP_DATA_API_DOMAIN_PATH + '/api/' + Utils.getSite() + '/kpi/Inverter?dateFrom='+ Utils.getLocalYYYYMMDD(this.state.startDate) +'&dateTo='+ Utils.getLocalYYYYMMDD(this.state.startDate)
        axios.get(API, { withCredentials: true })
            .then(res => {
                this.setState({
                    data: res.data
                })
                callback();
            })
            .catch(err => {
                Toaster({ title: 'Network Error', message: 'Error While Retrieving KPIs' }, 'error')
                this.setState({
                    data: [],
                })
                callback();
            })
    }

    parseData() {
        let groupDevices = {}
        var avgKPIs = []
        let overallSumPR = 0
        let overallSumYield = 0
        let overallPRCount =0
        let overallYieldCount =0
       
        this.state.data.forEach((inverters) => {
            groupDevices[inverters['device']] = groupDevices[inverters['device']] ? groupDevices[inverters['device']].concat([inverters]) : [inverters];
        });
            
        for (let device in groupDevices) {
            let sumPR = 0
            let sumYield = 0
            let sumKWH = 0
            let prCount=0
            let YieldCount=0
            let KWHCount=0
            let totalDowntime= 0
            let sumEfficiency = 0
            let efficiencyCount=0 
            for (var i = 0; i < groupDevices[device].length; i++) {
               
                if(groupDevices[device][i].kwhToday){
                    sumKWH += groupDevices[device][i].kwhToday;
                    KWHCount++
                }
                if(groupDevices[device][i].PR){
                    sumPR += groupDevices[device][i].PR;
                    overallSumPR += groupDevices[device][i].PR;
                    prCount++
                    overallPRCount++
                }
                if(groupDevices[device][i].yield){
                    sumYield += groupDevices[device][i].yield;
                    overallSumYield += groupDevices[device][i].yield;
                    YieldCount++
                    overallYieldCount++
                }
                if(groupDevices[device][i].downtime){
                    totalDowntime += groupDevices[device][i].downtime
                }
                if(groupDevices[device][i].irrEfficiency){
                    sumEfficiency += groupDevices[device][i].irrEfficiency
                    efficiencyCount++
                }
            }
            avgKPIs.push({ 'device': device,'downtime':totalDowntime, 'avgPR': prCount != 0 ? (sumPR / prCount) : 0, 'avgYield':YieldCount != 0 ? (sumYield / YieldCount) : 0, 'avgKWH':KWHCount != 0 ? (sumKWH / KWHCount) : 0 , 'sumKWH': sumKWH,'avgEfficiency': efficiencyCount != 0 ? (sumEfficiency / efficiencyCount) : 0, 'inverterKpiRecords': groupDevices[device] })
        }
        this.setState({
            avgKPIs: avgKPIs,
            overallAvgPR : overallSumPR/overallPRCount,
            overallAvgYield : overallSumYield/overallYieldCount
        }, () => {
            this.pickAndSort()
        })

    }

    pickAndSort() {
        let sortedArray = []
        let { sortBykpi, displayCount, avgKPIs } = this.state
        sortedArray =  avgKPIs.sort(function(a, b){
            return b[sortBykpi]-a[sortBykpi]
        })
        let firstSlice = sortedArray.slice(0, displayCount);
        let lastSlice = sortedArray.slice(sortedArray.length - displayCount, sortedArray.length);
        sortBykpi == 'downtime'?
        this.setState({
            hpInverters: lastSlice.reverse(),
            lpInverters: firstSlice
        }):
        this.setState({
            hpInverters: firstSlice,
            lpInverters: lastSlice.reverse()
        })
    }

    render() {
        const { lpInverters, hpInverters, sortBykpi} = this.state;
        return (      
                <Row>
                    <Col md={6} xl={6}>
                        <Card className='statistial-visit'>
                            <Card.Header>
                                <Card.Title as='h5'>High Performing Inverters</Card.Title>
                            </Card.Header>
                            <Card.Body className='pb-0' >
                            {
                                hpInverters ?
                                    hpInverters.map((invData, index) => {
                                        return <InverterKpiCard graphType={'bar'} key={invData.device} cardBorder={'card-border-c-green'} InverterKPI={invData} sortBykpi={sortBykpi} chartHeight={200} history={this.props.history}/>

                                    }) : 
                                    <div className="text-center">
                                        <Spinner />
                                    </div>
                            }
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} xl={6}>
                        <Card className='statistial-visit'>
                            <Card.Header>
                                <Card.Title as='h5'>Low Performing Inverters</Card.Title>
                            </Card.Header>
                            <Card.Body className='pb-0' >
                            {
                                    lpInverters ?
                                    lpInverters.map((invData, index) => {
                                        return <InverterKpiCard graphType={'bar'} key={invData.device} cardBorder={'card-border-c-red'} InverterKPI={invData} sortBykpi={sortBykpi} chartHeight={200} history={this.props.history}/>

                                    }) : 
                                    <div className="text-center">
                                    <Spinner />
                                </div>
                            }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
        );
    }
}

export default YesterdaysInvertersRanking;