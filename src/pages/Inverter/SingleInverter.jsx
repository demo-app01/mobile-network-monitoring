import React, { Component } from 'react';
import Utils from "../../Utils";
import { OverlayTrigger, Popover, Col, Card } from 'react-bootstrap';
// import {InverterSummaryCard} from '../InverterSummary/InverterSummary'
class SingleInverter extends Component {
    constructor() {
        super();
        this.overlay = React.createRef();
    }

    getColor() {
        let deviation = this.props.element.deviation;
        const { selectedDate } = this.props
        let  commTimeoutMins = 10 //Utils.getSiteMetaData(Utils.getSite())["commTimeoutMins"] || 10
        // if (selectedDate.toDateString() === new Date().toDateString()) {
        if (new Date().toDateString() === new Date().toDateString()) {
            if (this.props.element.PR == 100)
                return "inActive";
            else if (deviation > 1) return "green";
            else if (deviation >= 0 && deviation <= 1) return "light-green";
            else if (deviation >= -1 && deviation <= 0) return "yellow";
            else if (deviation >= -2 && deviation <= -1) return "light-yellow";
            else if (deviation <= -2) return "light-orange";   
            else return "inActive"   
        }else{
            if (deviation > 1) return "green";
            else if (deviation >= 0 && deviation <= 1) return "light-green";
            else if (deviation >= -1 && deviation <= 0) return "yellow";
            else if (deviation >= -2 && deviation <= -1) return "light-yellow";
            else if (deviation <= -2) return "light-orange";  
            else return "inActive"   
        }
    }

    _mouseEnter() {
        this.overlay.current.handleShow();
    }
    _mouseLeave() {
        this.overlay.current.handleHide();
    }

    render() {
        const { selectedDate } = this.props;
        if (this.props.element) {
            return (
                <OverlayTrigger ref={this.overlay} trigger="hover" rootClose={true} placement="top" overlay={
                    <Popover onMouseLeave={this._mouseLeave.bind(this)} onMouseEnter={this._mouseEnter.bind(this)} style={{ maxWidth: "100%" }} id="popover-positioned-top" className="shadow-lg inv-tooltip rounded-sm">
                        {/* <InverterSummaryCard Inv={this.props.element} headerColor={this.getColor()} /> */}
                    </Popover>}
                >
                    <div onClick={() => this.props.elementAction(this.props.element.device,this.props.element.name)} style={{ textAlign: "center" }} className={"square " + (this.getColor() ? this.getColor() :'default')}>
                        {selectedDate.toDateString() === new Date().toDateString() ?
                        ( this.props.element.ACPower <= 0 ? 
                            <i className="fa fa-exclamation-circle exclamation" /> : "" ) 
                        : null }
                    </div>
                </OverlayTrigger>
            );
        } else {
            return (
                <div onClick={() => this.props.elementAction(this.props.element)} style={{ textAlign: "center" }} className={"square " + this.getColor()}>
                    { selectedDate.toDateString() === new Date().toDateString() ?
                        (this.props.element.ACPower <= 0 ? <i className="fa fa-exclamation-circle exclamation"></i> : "") : null
                    }
                </div>)
        }
    }
}



export default SingleInverter;