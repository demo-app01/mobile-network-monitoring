import React from 'react';
import {Card} from 'react-bootstrap';
import PlantLegend from '../portfolio/PlantLegend';

function PanelwithName(props) {
    return (
        <Card>
            <Card.Header>
                <Card.Title as='h5'>{props.name}</Card.Title>
                {props.legend}
            </Card.Header>
            <Card.Body>
                {props.children}
            </Card.Body>
        </Card>
    );
}

export default PanelwithName;