import React, { Component } from 'react';
import Utils from "../Utils";
import moment from 'moment';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import "bootstrap-daterangepicker/daterangepicker.css";
import Select from 'react-select';
import { Link } from "react-router-dom";
import {
    Row,
    Col,
    Button,
    Breadcrumb,
} from 'react-bootstrap';
class BreadCrumb extends Component {

    constructor(props) {
        super(props);

        var deviceOptions = [];
        
        this.props.isDeviceDropDown &&
        this.props.hardwareDevices.map((element, index) =>
            deviceOptions.push({'key' : index, 'value': element, 'label': Utils.getDeviceName(element) })  
        )

        this.state = {
            deviceOptions : deviceOptions
        }
    }
    
    
    componentWillMount(){
        let path = window.location.pathname.split("/")
        // if(path[1]==='user-management'){
        //     this.setState({userAccess:Utils.getuserAccess(),plantName : 'User Management'});
        // }else{
        //     this.setState({userAccess:Utils.getuserAccess(),plantName : Utils.getSiteMetaData(Utils.getSite())["plantDisplayName"]});

        // }
}

    render() {
       
        const {inverterRanking,startDate,yearOptions,maxDate,endDate,handleCustomDate,onYearChange,isDeviceDropDown,isSingleDatePicker,isMultiDatePicker,ranges,deviceName,handleChange,moduleName,selectedYear,yearSelection,subModuleName,userManagementPath} = this.props
        const {userAccess,plantName } = this.state
       
        return (
            <div className="page-header">
                <div className="page-block">
                    <Row className="align-items-center">
                        <Col md={12} lg={12}>
                            <div className="page-header-title m-t-30">
                                <h5 className="m-b-10 float-left">{subModuleName ? subModuleName : moduleName}</h5>

                                {
                                    yearSelection &&
                                    <div className="float-right" style={{ width: '100px' }}>
                                        <Select
                                            value={selectedYear}
                                            onChange={onYearChange}
                                            options={yearOptions}
                                        />
                                    </div>
                                }
                                {
                                    isSingleDatePicker && (
                                        <DateRangePicker
                                            singleDatePicker={true}
                                            startDate={startDate}
                                            minDate='01/01/2012'
                                            maxDate={maxDate}
                                            showWeekNumbers={false}
                                            opens={'left'}
                                            showDropdowns={true}
                                            onApply={handleCustomDate}
                                            containerClass={'float-right'}
                                            timezone= {( Utils.getSiteMetaData(Utils.getSite()) && Utils.getSiteMetaData(Utils.getSite())["timeZone"] )? moment.tz.setDefault(Utils.getSiteMetaData(Utils.getSite())["timeZone"]):moment.tz.setDefault("Asia/Kolkata")}
                                        >
                                            <Button className='m-0 btn-sm mt-0'>
                                                <i className={'feather icon-calendar'} />
                                                {" " + moment(startDate).format('DD-MMM-YYYY')}<b className="fa fa-caret-down ml-2"></b>
                                            </Button>
                                        </DateRangePicker>
                                    )
                                }

                                {
                                    isMultiDatePicker && (
                                        <DateRangePicker
                                            startDate={startDate}
                                            endDate={endDate}
                                            minDate='01/01/2012'
                                            maxDate={maxDate}
                                            showWeekNumbers={false}
                                            opens={'left'}
                                            showDropdowns={true}
                                            onApply={handleCustomDate}
                                            ranges={ranges}
                                            containerClass={'float-right'}
                                            timezone= {moment.tz.setDefault("Asia/Kolkata")}// {( Utils.getSiteMetaData(Utils.getSite()) && Utils.getSiteMetaData(Utils.getSite())["timeZone"] )? moment.tz.setDefault(Utils.getSiteMetaData(Utils.getSite())["timeZone"]):moment.tz.setDefault("Asia/Kolkata")}
                                        >
                                            <Button className='m-0 btn-sm mt-0 float-right'>
                                                <i className={'feather icon-calendar'} />
                                                {moment(this.props.startDate).format('DD-MM-YYYY') === moment(this.props.endDate).format('DD-MM-YYYY') ? moment(this.props.startDate).format('DD-MMM-YYYY') : 'From ' + moment(this.props.startDate).format('DD-MMM-YYYY') + ' To ' + moment(this.props.endDate).format('DD-MMM-YYYY')} <b className="fa fa-caret-down ml-2"></b>
                                            </Button>
                                        </DateRangePicker>
                                    )
                                }

                                {inverterRanking && (userAccess && userAccess.includes("INV:ranking") ? <Link to={"/" + Utils.getSite() + '/inverter/rankings'}>
                                    <button className="m-r fontcolor"  >View Inverter Rankings</button>
                                </Link> : '')
                                }
                                {
                                    isDeviceDropDown && deviceName ? (
                                        <div className="col-md-3 col-xs-12 float-right">

                                            <Select
                                                onChange={handleChange}
                                                value={deviceName}
                                                name="deviceName"
                                                options={this.state.deviceOptions}
                                            />

                                        </div>
                                    ) : null
                                }
                            </div>
                        </Col>
                        {
                            plantName == 'User Management' ?
                                <Breadcrumb className='mr-auto'>
                                    <Breadcrumb.Item href={'#'}><i className="feather icon-home" /></Breadcrumb.Item>
                                    <Link to={"/" + Utils.getSite() + '/dashboard'}><Breadcrumb.Item href={'#'}>{plantName}</Breadcrumb.Item></Link>
                                    <Link to={userManagementPath}><Breadcrumb.Item href={'#'}>{moduleName}</Breadcrumb.Item></Link>
                                    {subModuleName && <Breadcrumb.Item active>{subModuleName}</Breadcrumb.Item>}
                                </Breadcrumb>
                                :
                                <Breadcrumb className='mr-auto'>
                                    <Breadcrumb.Item href={'#'}><i className="feather icon-home" /></Breadcrumb.Item>
                                    {/* <Breadcrumb.Item href={'#'}>
                                        <Link to={"/" + Utils.getSite() + '/dashboard'}>
                                            {plantName}
                                        </Link>
                                    </Breadcrumb.Item> */}
                                    <Breadcrumb.Item active>{moduleName}</Breadcrumb.Item>
                                </Breadcrumb>
                        }
                    </Row>
                </div>
            </div>

        );
    }
}

export default BreadCrumb;